Feature: Group management
  As an user
  In order to manage things with different people and scopes
  I want to be able to create groups

  Background:
    Given an user with email "jane@doe.com"
    And an user with email "max@friends.com"
    And I have an active account for "john@doe.com"
    And I have a "Friends" group
    And I have a "Home" group
    And I joined a "Friends of Carlotta" group
    And I joined 1 other groups
    And I have a "Safe box" container in the "Home" group
    Given user with email "max@friends.com" has joined my "Friends" group
    Given I authenticate as "john@doe.com"

  Scenario: Accessing the group list
    When I click on "Manage groups" in the main menu
    Then I see the group list

  Scenario: Create a group
    Given I access the groups list
    When I access the new group form
    And I fill the group form for a "Work" group
    Then I see "Work" in the groups list

  Scenario: Edit a group
    Given I access the groups list
    When I edit the "Friends" group
    And I change the group name to "Foes"
    Then I see "Foes" in the groups list

  Scenario: List groups
    When I access the groups list
    Then I see 5 groups

  Scenario: Actions on non-owned group
    When I access the groups list
    Then I can't destroy the "Friends of Carlotta" group
    Then I can't edit the "Friends of Carlotta" group
    Then I can leave the "Friends of Carlotta" group

  Scenario: Actions on owned group
    When I access the groups list
    Then I can destroy the "Home" group
    Then I can edit the "Home" group
    Then I can't leave the "Home" group

  Scenario: Destroy an inactive, owned group
    Given I manage the "Friends" group
    When I access the groups list
    And I destroy the group named "Home"
    Then I don't see the "Home" group in the list

  Scenario: Destroy an active, owned group
    Given I manage the "Home" group
    When I access the groups list
    And I destroy the group named "Home"
    Then I don't see the "Home" group in the list
    And I see a message stating "Choose a group to use"

  Scenario: Leave an inactive, non-owned group
    Given I manage the "Home" group
    When I access the groups list
    And I leave the "Friends of Carlotta" group
    Then I don't see the "Friends of Carlotta" group in the list

  Scenario: Leave an active, non-owned group
    Given I manage the "Friends of Carlotta" group
    When I access the groups list
    And I leave the "Friends of Carlotta" group
    Then I don't see the "Friends of Carlotta" group in the list

  Scenario: Destroy and leave all the group
    Given I access the groups list
    When I destroy and leave all the groups
    Then I see a message stating "No group available. Create one"
    When I sign out
    And I authenticate as "john@doe.com"
    Then I see a message stating "No group available. Create one"

  Scenario: Change the current group
    Given I change the current group to "Home"
    Then the current group is "Home"
    And I see "Safe box" in the containers list
    When I change the current group to "Friends"
    Then the current group is "Friends"
    Then I don't see the "Safe box" container in the list

  Scenario: See users in the group
    Given user with email "jane@doe.com" has joined my "Home" group
    And I manage the "Home" group
    When I access the groups list
    And I expand the current group
    Then I see a form to invite an user
    And I see 2 users in the current group

  Scenario: Invite user
    Given I manage the "Home" group
    And I display details on the current group
    When I invite user with email "jane@doe.com"
    Then I see 2 users in the current group

  Scenario: Remove user from group
    And I manage the "Friends" group
    And I display details on the current group
    When I remove user with email "max@friends.com"
    And I see 1 user in the current group

  Scenario: Behavior of the group form
    And I access the groups list
    # First opening
    And I access the new group form
    Then the group form is empty
    When I create a "Bungalow" group
    # Opening after creation
    And I access the new group form
    Then the group form is empty again
    # Opening for edition
    When I create a "Flat" group
    When I edit the "Bungalow" group
    Then the group form is filled with the "Bungalow" data
    And I change the group name to "House"
    # Opening after edition
    And I access the new group form
    Then the group form is empty again
