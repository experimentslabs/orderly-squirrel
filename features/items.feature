Feature: Items management
  As an user
  In order to sort my things
  I want to create, update and destroys items
  I want all items to be scoped to a group

  Background:
    Given I have an active account for "jane@doe.com"
    Given I authenticate as "jane@doe.com"

  Scenario: Create an item
    Given I click on "Items" in the main menu
    And I access the new item form
    When I fill the item form for a "French baguette" item
    Then I see "French baguette" in the items list

  Scenario: Edit an item
    Given I have a "Camembert" item
    And I access the items list
    When I edit the "Camembert" item
    And I change the item name to "Camembert - Le parfait"
    Then I see "Camembert - Le parfait" in the items list

  Scenario: List items
    Given I have 3 items in the current group
    When I access the items list
    Then I see 3 items

  Scenario: Destroy an item
    Given I have a "Cat food" item
    And I access the items list
    When I destroy the item named "Cat food"
    Then I don't see the "Cat food" item in the list

  Scenario: Empty list
    Given I have no item in the current group
    And I access the items list
    Then I see no item in the list

  Scenario: Add item to shopping list
    Given I have a "Groceries" shopping list
    Given I have a "Camembert" item
    And I access the items list
    When I add the "Camembert" to "Groceries"
    And I access the "Groceries" shopping list
    Then I see the "Camembert" item in the shopping list

  Scenario: Behavior of the item form
    Given I access the items list
    # First opening
    And I access the new item form
    Then the item form is empty
    When I create a "AAA battery" item
    # Opening after creation
    And I access the new item form
    Then the item form is empty again
    # Opening for edition
    When I create a "9-Volt" item
    When I edit the "AAA battery" item
    Then the item form is filled with the "AAA battery" data
    And I change the item name to "R03"
    # Opening after edition
    When I access the new item form
    Then the item form is empty again
