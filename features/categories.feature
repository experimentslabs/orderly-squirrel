Feature: Categories management
  As an user
  In order to sort my things
  I want to assign them a category
  I want all categories to be scoped to a group

  Background:
    Given I have an active account for "philemon@osquirrel.com"
    Given I authenticate as "philemon@osquirrel.com"

  Scenario: Create a category
    Given I click on "Categories" in the main menu
    When I access the new category form
    And I fill the category form for a "Food" category
    Then I see "Food" in the categories list

  Scenario: Edit a category
    Given I have a "CDs" category
    And I access the categories list
    When I edit the "CDs" category
    And I change the category name to "Old things"
    Then I see "Old things" in the categories list

  Scenario: List categories
    Given I have 3 categories in the current group
    When I access the categories list
    Then I see 3 categories

  Scenario: Destroy a category
    Given I have a "CDs" category
    And I access the categories list
    When I destroy the category named "CDs"
    Then I don't see the "CDs" category in the list

  Scenario: Empty list
    Given I have no category in the current group
    And I access the categories list
    Then I see no category in the list

  Scenario: Behavior of the category form
    And I access the categories list
    # First opening
    And I access the new category form
    Then the category form is empty
    When I create a "Vegetable" category
    # Opening after creation
    And I access the new category form
    Then the category form is empty again
    # Opening for edition
    When I create a "Body care" category
    When I edit the "Vegetable" category
    Then the category form is filled with the "Vegetable" data
    And I change the category name to "Greenies"
    # Opening after edition
    And I access the new category form
    Then the category form is empty again
