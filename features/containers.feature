Feature: Container management
  As an user
  In order to sort my things
  I want to assign them to containers
  I want all containers to be scoped to a group

  Background:
    Given I have an active account for "sofi@example.com"
    Given I authenticate as "sofi@example.com"

  Scenario: Create a container
    Given I access the containers list
    And I access the new container form
    When I fill the container form for a "Drawer" container
    Then I see "Drawer" in the containers list

  Scenario: Edit a container
    Given I have a "Freezer" container
    And I access the containers list
    When I edit the "Freezer" container
    And I change the container name to "Kitchen shelf"
    Then I see "Kitchen shelf" in the containers list

  Scenario: List containers
    Given I have 3 containers in the current group
    When I access the containers list
    Then I see 3 containers

  Scenario: Destroy a container
    Given I have a "Fridge" container
    And I access the containers list
    When I destroy the container named "Fridge"
    Then I don't see the "Fridge" container in the list

  Scenario: Empty list
    Given I have no container in the current group
    And I access the containers list
    Then I see no container in the list

  Scenario: Add item to container
    Given I have a "Fridge" container
    And I have a "Butter" item
    And I access the containers list
    When I add the "Butter" in the "Fridge"
    Then I see the "Butter" item in the "Fridge" container

  Scenario: Remove item from container
    Given I have a "Fridge" container with some "Lettuce" in it
    And I access the containers list
    When I remove the "Lettuce" from the "Fridge"
    Then I don't see the "Lettuce" item in the "Fridge" container

  Scenario: Add amount of an item in a container
    Given I have a "Fridge" container with 3 "Lettuce" in it
    And I access the containers list
    When I add 1 "Lettuce" in the "Fridge" container
    Then I see 4 "Lettuce" items in the "Fridge" container

  Scenario: Remove amount of an item in a container
    Given I have a "Fridge" container with 3 "Lettuce" in it
    And I access the containers list
    When I remove 1 "Lettuce" from the "Fridge" container
    Then I see 2 "Lettuce" items in the "Fridge" container

  Scenario: Behavior of the item/container form
    Given I have a "Fridge" container
    And I have a "Butter" item
    And I access the containers list
    # First opening
    When I open the form to add an item in the "Fridge"
    Then the container-item form for "Fridge" is empty
    # Opening after creation
    When I add the "Butter" in the "Fridge"
    And I open the form to add an item in the "Fridge"
    Then the container-item form for "Fridge" is empty again
    # Opening for edition
    When I open the form to edit the "Butter" in the "Fridge" container
    Then the container-item form is filled with the "Fridge" and "Butter" data
    # Opening after edition
    When I open the form to add an item in the "Fridge"
    Then the container-item form for "Fridge" is empty again

  Scenario: Behavior of the container form
    Given I access the containers list
    # First opening
    And I access the new container form
    Then the container form is empty
    When I create a "Secret stash" container
    # Opening after creation
    And I access the new container form
    Then the container form is empty again
    # Opening for edition
    When I create a "Super secret stash" container
    When I edit the "Secret stash" container
    Then the container form is filled with the "Secret stash" data
    And I change the container name to "Under the rug"
    # Opening after edition
    And I access the new container form
    Then the container form is empty again
