Feature: Home page
  As an user
  In order to manage my things easily
  I want to see the containers of the last used group as a home page

  Scenario: Homepage displays the containers
    Given I have an active account for "barbara@osquirrel.com"
    When I authenticate as "barbara@osquirrel.com"
    Then I see the containers page
