require 'simplecov'
require 'capybara-screenshot/cucumber'

browser = ENV['CAPYBARA_DRIVER'] || 'firefox-headless'

Capybara.register_driver(:selenium) do |app|
  if %w[chrome chrome-headless].include? browser
    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    # Chrome 75 defaults to W3C mode, which doesn't specify a way to get log access.
    # We need to fallback to "old" mode
    # FIXME: Remove this when Chrome supports log access
    caps[:chromeOptions] = { w3c: false }
    # More on the logging prefs levels: http://www.tjmaher.com/2018/12/capybara-gauge-6.html
    caps[:logging_prefs] = { browser: 'ALL' }
  else
    # FIXME: Find a way to log JS from Firefox
    caps = Selenium::WebDriver::Remote::Capabilities.firefox
  end

  client = Selenium::WebDriver::Remote::Http::Default.new
  # Wait longer
  client.timeout = 120

  common_options = { desired_capabilities: caps, http_client: client }

  browser_options = {
    'chrome'           => { browser: :chrome },
    'chrome-headless'  => { browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless no-sandbox]) },
    'firefox'          => { browser: :firefox },
    'firefox-headless' => { browser: :firefox, options: Selenium::WebDriver::Firefox::Options.new(args: %w[--headless]) },
  }

  Capybara::Selenium::Driver.new(app, browser_options[browser].merge(common_options))
end

Capybara.default_driver    = :selenium
Capybara.javascript_driver = :selenium
Capybara.save_path         = File.join('features', 'capybara-screenshots')

# We're on front/api, give us some time
Capybara.default_max_wait_time = 10

# Capybara screenshot
Capybara::Screenshot.prune_strategy = :keep_last_run

# Only log console output when using Chrome
if %w[chrome chrome-headless].include?(browser) && (ENV['LOG_JS'] == 'true')
  AfterStep do |_scenario, _step|
    if page.driver.browser.respond_to?(:manage)
      logs = page.driver.browser.manage.logs.get(:browser)

      failures = 0

      logs.each do |log|
        failures += 1 if log.level != 'INFO'
        puts "[JS] #{log.message}\n\n"
      end

      raise DriverJSError, "#{failures} JS errors occured" if failures.positive?
    end
  end
end

class DriverJSError < StandardError
end
