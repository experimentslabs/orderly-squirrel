Feature: Item thresholds
  As an user
  In order to manage easily important items
  I want to have an alert when the amount of an item is below a given threshold

  Background:
    Given I have an active account for "sofi@example.com"
    And I authenticate as "sofi@example.com"

  Scenario: Threshold passed in containers
    And I have a "Ice cream" item with a minimum quantity of 1
    And I have a "Freezer" container with the following items
      | Ice cream | 2 |
    And I access the containers list
    When I remove 1 "Ice cream" from the "Freezer" container
    Then I see a warning concerning the "Ice cream" in the "Freezer"

  Scenario: Threshold passed in items list
    And I have a "Ice cream" item with a minimum quantity of 1
    And I have a "Freezer" container with the following items
      | Ice cream | 0 |
    And I access the items list
    Then I see a warning concerning the "Ice cream" item
