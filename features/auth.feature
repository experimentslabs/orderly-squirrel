Feature: Account
  As a visitor
  In order to use the application
  I want to create an account
  I want be able to manage this account

  Scenario: Creating an account
    Given I'm not authenticated
    When I create an account for "me@example.com"
    Then I see a message stating "A message with a confirmation link has been sent to your email address."

  Scenario: Signing in
    Given I'm not authenticated
    And I have an active account for "me@example.com"
    When I authenticate as "me@example.com"
    Then I see the application

  Scenario: Signing out
    And I have an active account for "me@example.com"
    Given I authenticate as "me@example.com"
    When I sign out
    Then I see the login form
