Feature: Shopping list management
  As an user
  In order to sort my things
  I want to assign them to shopping lists
  I want all shopping lists to be scoped to a group

  Background:
    Given I have an active account for "sofi@example.com"
    Given I authenticate as "sofi@example.com"

  Scenario: Create a shopping list
    Given I access the shopping lists
    And I access the new shopping list form
    When I fill the shopping list form for a "Groceries for the week" shopping list
    Then I see "Groceries for the week" in the shopping lists

  Scenario: Edit a shopping list
    Given I have a "Tom's birthday" shopping list
    And I access the shopping lists
    When I edit the "Tom's birthday" shopping list
    And I change the shopping list name to "Tom's funerals"
    Then I see "Tom's funerals" in the shopping lists

  Scenario: List shopping lists
    Given I have 3 shopping lists in the current group
    When I access the shopping lists
    Then I see 3 shopping lists

  Scenario: Destroy a shopping list
    Given I have a "Family party" shopping list
    And I access the shopping lists
    When I destroy the shopping list named "Family party"
    Then I don't see the "Family party" shopping list in the list

  Scenario: Empty list
    Given I have no shopping list in the current group
    And I access the shopping lists
    Then I see no shopping list in the list

  Scenario: Display a shopping list details
    Given I have a "Family party" shopping list
    And I access the shopping lists
    When I click on "Family party"
    Then I see the "Family party" shopping list details

  Scenario: Add item to shopping list
    Given I have a "Groceries" shopping list
    And I have a "Butter" item
    And I access the "Groceries" shopping list
    When I add the "Butter" in the shopping list
    Then I see the "Butter" item in the shopping list

  Scenario: Remove item from shopping list
    Given I have a "Buy fruits" shopping list with the following items
      | Apple | 1 |  |
    And I access the "Buy fruits" shopping list
    When I remove the "Apple" from the shopping list
    Then I don't see the "Apple" item in the shopping list

  Scenario: Amount of done items
    Given I have a "Groceries" shopping list with the following items
      | Cucumber | 1 |      |
      | Lettuce  | 1 |      |
      | Threat   | 1 | done |
    When I access the shopping lists
    Then I see a shopping list with 3 items and 1 done

  Scenario: Mark an item as done
    Given I have a "Groceries" shopping list with the following items
      | Cucumber | 1 |      |
      | Threat   | 1 | done |
    When I access the "Groceries" shopping list
    And I mark the the "Cucumber" item as done
    Then The "Cucumber" item is marked as done

  Scenario: Uncheck an item as done
    Given I have a "Groceries" shopping list with the following items
      | Cucumber | 1 |      |
      | Threat   | 1 | done |
    When I access the "Groceries" shopping list
    And I mark the the "Threat" item as undone
    Then The "Threat" item is marked as undone

  Scenario: Complete a shopping list
    Given I have a "Groceries" shopping list with the following items
      | Cucumber | 1 | done |
      | Threat   | 1 | done |
    When I access the "Groceries" shopping list
    And I mark the shopping list as done
    Then the shopping list "Groceries" is marked as done

  Scenario: Assign user to a shopping list item
    Given user "JaneDoe" has joined my group
    And I have a "Groceries" shopping list with the following items
      | Cucumber | 1 | todo |
      | Threat   | 1 | todo |
    # Reload users
    And I access the groups list
    And I access the "Groceries" shopping list
    When I assign "JaneDoe" to to pick the "Cucumber"
    Then I see that "JaneDoe" is assigned to pick the "Cucumber"

  Scenario: Behavior of the item/shopping list form
    Given I have a "Birthday" shopping list
    And I have a "Butter" item
    And I access the "Birthday" shopping list
    # First opening
    When I open the form to add an item in the shopping list
    Then the shopping_list-item form for "Birthday" is empty
    # Opening after creation
    When I add the "Butter" in the shopping list
    And I open the form to add an item in the shopping list
    Then the shopping_list-item form for "Birthday" is empty again
    # Opening for edition
    When I open the form to edit the "Butter" in the "Birthday" shopping list
    Then the shopping_list-item form is filled with the "Birthday" and "Butter" data
    # Opening after edition
    When I open the form to add an item in the shopping list
    Then the shopping_list-item form for "Birthday" is empty again

  Scenario: Behavior of the shopping list form
    Given I access the shopping lists
    # First opening
    And I access the new shopping list form
    Then the shopping list form is empty
    When I create a "Pizzas" shopping list
    # Opening after creation
    And I access the new shopping list form
    Then the shopping list form is empty again
    # Opening for edition
    When I create a "Week errands" shopping list
    When I edit the "Pizzas" shopping list
    Then the shopping list form is filled with the "Pizzas" data
    And I change the shopping list name to "Soups"
    # Opening after edition
    And I access the new shopping list form
    Then the shopping list form is empty again
