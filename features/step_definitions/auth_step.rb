Given('I authenticate as {string}') do |email|
  step "I authenticate as \"#{email}\" with password \"password\""
end

Given('I authenticate as {string} with password {string}') do |email, password|
  visit '/'
  fill_in 'Email', with: email
  fill_in 'Password', with: password

  click_on 'Log in'

  @current_user = User.find_by(email: email)
  @current_group_name = nil

  expect(page).to have_content 'Welcome back'
end

Given("I'm not authenticated") do
  begin
    find('nav.navbar')
    step 'I sign out'
  rescue Capybara::ElementNotFound # rubocop:disable Lint/SuppressedException
  end
  true
end

Given("I'm authenticated as an user with email {string}") do |email|
  FactoryBot.create :user_active, email: email

  step "I authenticate as \"#{email}\""
end

Given('I have an active account for {string}') do |email|
  @current_user = FactoryBot.create :user_active, email: email
end

Then('I see the application') do
  expect(page).to have_content 'Containers'
end

When('I sign out') do
  step 'I click on "Logout" in the main menu'
  @current_user = nil
  step 'I see the login form'
end

Then('I see the login form') do
  expect(page).to have_content('Log in')
end

When('I create an account for {string}') do |email|
  name = email.sub(/@.*/, '')

  visit '/'

  click_on 'Sign up'

  fill_in 'Name', with: name
  fill_in 'Email', with: email
  fill_in 'Password', with: 'password'
  fill_in 'Password confirmation', with: 'password'

  click_on 'Sign up'
end

Then('I see a message stating {string}') do |message|
  expect(page).to have_content message
end

Given('an user with email {string}') do |email|
  FactoryBot.create :user_active, email: email
end
