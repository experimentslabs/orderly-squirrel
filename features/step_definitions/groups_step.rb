Then('I see the group list') do
  expect(find('.toolbar .toolbar__title')).to have_content 'Groups'
end

Given('I access the new group form') do
  within('header.toolbar') do
    click_on 'Add'
  end

  expect(page).to have_content('New group')
end

Given('I fill the group form for a {string} group') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see {string} in the groups list') do |name|
  expect(find('.groups')).to have_content name
end

When('I create a {string} group') do |name|
  step 'I access the new group form'
  step "I fill the group form for a \"#{name}\" group"
end

Given('I have a {string} group') do |name|
  FactoryBot.create :group, creator: @current_user, name: name
end

Given('I access the groups list') do
  step 'I click on "Manage groups" in the main menu'
end

When('I edit the {string} group') do |name|
  within('.group', text: /^#{name}$/) do
    click_on 'Edit'
  end

  expect(page).to have_content 'Group edition'
end

When('I change the group name to {string}') do |name|
  step "I fill the group form for a \"#{name}\" group"
end

Given(/I joined (\d+) other groups?$/) do |amount|
  FactoryBot.create_list :group, amount, users: [@current_user]
end

Then('I see {int} groups') do |amount|
  within('.groups') do
    current_scope.assert_selector('.group', count: amount)
  end
end

When('I destroy and leave all the groups') do
  find_all('.group').each do |group|
    within(group) do
      click_on 'Show more actions'
      if find_all('button:not([disabled])', text: 'Destroy').count == 1
        click_on 'Destroy'
      elsif find_all('button:not([disabled])', text: 'Leave').count == 1
        click_on 'Leave'
      else
        find('.dropdown__overlay').click
      end
    end
  end

  page.assert_selector('.group', count: 0)
end

When('I destroy the group named {string}') do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    click_on 'Destroy'
  end
end

When('I leave the {string} group') do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    click_on 'Leave'
  end
end

Then("I don't see the {string} group in the list") do |name|
  expect(find('.groups')).not_to have_content name
end

Given('I joined a {string} group') do |name|
  FactoryBot.create :group, name: name, users: [@current_user]
end

Then('I can destroy the {string} group') do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    current_scope.assert_selector('button:not([disabled])', text: 'Destroy', count: 1)
    find('.dropdown__overlay').click
  end
end

Then("I can't destroy the {string} group") do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    current_scope.assert_selector('button[disabled]', text: 'Destroy', count: 1)
    find('.dropdown__overlay').click
  end
end

Then('I can edit the {string} group') do |name|
  within('.group', text: name) do
    current_scope.assert_selector('button:not([disabled])[title="Edit"]', count: 1)
  end
end

Then("I can't edit the {string} group") do |name|
  within('.group', text: name) do
    current_scope.assert_selector('button[disabled][title="Edit"]', count: 1)
  end
end

Then('I can leave the {string} group') do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    current_scope.assert_selector('button:not([disabled])', text: 'Leave', count: 1)
    find('.dropdown__overlay').click
  end
end

Then("I can't leave the {string} group") do |name|
  within('.group', text: name) do
    click_on 'Show more actions'
    current_scope.assert_selector('button[disabled]', text: 'Leave', count: 1)
    find('.dropdown__overlay').click
  end
end

Given('I have a {string} container in the {string} group') do |container_name, group_name|
  group = Group.find_by(name: group_name)
  FactoryBot.create :container, name: container_name, group: group, user: @current_user
end

When('I change the current group to {string}') do |name|
  step 'open main menu'

  click_on 'Change group…'
  find('.group-picker__list__item', text: /^#{name}$/).click
end

Then('the current group is {string}') do |name|
  expect(find('nav.navbar .navbar__main')).to have_content name
end

Given('I manage the {string} group') do |name|
  step "I change the current group to \"#{name}\""

  @current_group_name = name
end

Then(/^the group form is empty(?: again)?$/) do
  expect(page.find_field('Name').value).to eq ''

  click_on 'Cancel'
end

Then('the group form is filled with the {string} data') do |name|
  expect(page.find_field('Name').value).to eq name
end

Given('user with email {string} has joined my {string} group') do |email, group_name|
  user  = User.find_by email: email
  group = Group.find_by name: group_name

  group.users.push user
end

When('I expand the current group') do
  find('.group.element .element__header__title', text: /^#{@current_group_name}$/).click
end

Then('I see a form to invite an user') do
  expect(page).to have_content 'Invite!'
end

Then(/^I see (\d+) users? in the current group$/) do |amount|
  within('.group', text: /^#{@current_group_name}$/) do
    current_scope.assert_selector('.user.list-element', count: amount)
  end
end

Given('I display details on the current group') do
  step 'I access the groups list'
  step 'I expand the current group'
end

When('I invite user with email {string}') do |email|
  fill_in 'Email', with: email

  click_on 'Invite!'
end

When('I remove user with email {string}') do |email|
  user = User.find_by email: email

  within('.user.list-element', text: user.name) do
    click_on 'Remove'
  end
end

Given('user {string} has joined my group') do |name|
  FactoryBot.create :user_active, groups: [@current_user.groups.first], name: name
end
