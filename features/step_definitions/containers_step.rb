Given('I access the new container form') do
  within('header.toolbar') do
    click_on 'Add'
  end

  expect(page).to have_content('New container')
end

Given('I fill the container form for a {string} container') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see {string} in the containers list') do |name|
  within('.containers') do
    expect(current_scope).to have_content name
  end
end

When('I create a {string} container') do |name|
  step 'I access the new container form'
  step "I fill the container form for a \"#{name}\" container"
end

Given('I have a {string} container') do |name|
  group = @current_user.groups.first

  FactoryBot.create :container, group: group, user: @current_user, name: name
end

# Note: items should be created first
Given('I have a {string} container with the following items') do |container, table|
  data = table.raw

  container = FactoryBot.create :container, name: container, group: @current_user.groups.first

  data.each do |row|
    item = Item.find_by name: row[0], group: @current_user.groups.first

    FactoryBot.create :containers_item, item: item, container: container, quantity: row[1]
  end
end

Given('I access the containers list') do
  visit '/'
end

When('I destroy the container named {string}') do |name|
  within('.container', text: name) do
    click_on 'Show more actions'
    click_on 'Destroy'
  end
end

Then("I don't see the {string} container in the list") do |name|
  within('.containers') do
    expect(current_scope).not_to have_content name
  end
end

Given('I have {int} containers in the current group') do |amount|
  group = @current_user.groups.first

  FactoryBot.create_list :container, amount, group: group, user: @current_user
end

Then('I see {int} containers') do |amount|
  within('.containers') do
    current_scope.assert_selector('.container', count: amount)
  end
end

When('I edit the {string} container') do |name|
  within('.container', text: name) do
    click_on 'Show more actions'
    click_on 'Edit'
  end

  expect(page).to have_content 'Container edition'
end

When('I change the container name to {string}') do |name|
  step "I fill the container form for a \"#{name}\" container"
end

Given('I have no container in the current group') do
  group = @current_user.groups.first
  group.containers.destroy_all
end

Then('I see no container in the list') do
  within('.containers') do
    current_scope.assert_selector('.container', count: 0)

    expect(current_scope).to have_content 'You have no container for now'
  end
end

Then('I see the containers page') do
  expect(find('.toolbar')).to have_content 'Containers'
end

When(/^I add (\d+|the) "(.+)" in the "(.+)"$/) do |amount, item, container|
  quantity = amount == 'the' ? 1 : amount.to_i

  step "I open the form to add an item in the \"#{container}\""

  fill_in 'Item', with: item
  click_on item

  fill_in 'Quantity', with: quantity

  click_on 'Save'
end

Then('I see the {string} item in the {string} container') do |item, container|
  within('.container', text: container) do
    expect(current_scope).to have_content item
  end
end

Given(/^I have a "(.+)" container with (\d+|some) "(.+)" in it$/) do |container, amount, item|
  quantity  = amount == 'some' ? 1 : amount.to_i
  container = FactoryBot.create :container, name: container, group: @current_user.groups.first
  item      = FactoryBot.create :item, name: item, group: @current_user.groups.first
  FactoryBot.create :containers_item, container: container, item: item, quantity: quantity
end

When('I remove the {string} from the {string}') do |item, _container|
  within('.container .item', text: item) do
    click_on 'Show more actions'
    click_on 'Remove'
  end
end

Then("I don't see the {string} item in the {string} container") do |item, container|
  within('.container', text: container) do
    expect(current_scope).not_to have_content item
  end
end

Then('I see {int} {string} items in the {string} container') do |amount, item, container|
  within('.container', text: container) do
    within('.item', text: item) do
      expect(current_scope).to have_content amount
    end
  end
end

When(/^I (add|remove) (\d+) "(.+)" (?:in|from) the "(.+)" container$/) do |action, amount, item, container|
  button_string = action == 'add' ? '+' : '-'

  within('.container', text: container) do
    within('.item', text: item) do
      amount.times { click_on button_string }
    end
  end
end

When('I open the form to add an item in the {string}') do |container|
  within('.container', text: container) do
    click_on 'Add item'
  end
end

Then(/^the container-item form for "(.+)" is empty(?: again)?$/) do |container|
  expect(page.find_field('Container', disabled: true).value).to eq container
  expect(page.find_field('Item').value).to eq ''
  expect(page.find_field('Quantity').value).to eq '1'
  click_on 'Cancel'
end

When('I open the form to edit the {string} in the {string} container') do |item, container|
  within('.container', text: container) do
    within('.item', text: item) do
      click_on 'Show more actions'
      click_on 'Edit'
    end
  end
end

Then('the container-item form is filled with the {string} and {string} data') do |container, item|
  db_item = Item.find_by name: item
  db_container = Container.find_by name: container
  association = ContainersItem.find_by container: db_container, item: db_item

  expect(page.find_field('Container').value).to eq container
  page.assert_selector('label', text: 'Item', count: 0)
  expect(page.find_field('Quantity').value).to eq association.quantity.to_s

  click_on 'Cancel'
end

Then(/^the container form is empty(?: again)?$/) do
  expect(page.find_field('Name').value).to eq ''

  click_on 'Cancel'
end

Then('the container form is filled with the {string} data') do |name|
  expect(page.find_field('Name').value).to eq name
end

Then('I see a warning concerning the {string} in the {string}') do |item, container|
  within('.container', text: container) do
    current_scope.assert_selector('.item .list-element__content.danger', text: item, count: 1)
  end
end
