Given('I access the new category form') do
  within('header.toolbar') do
    click_on 'Add'
  end

  expect(page).to have_content('New category')
end

Given('I fill the category form for a {string} category') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see {string} in the categories list') do |name|
  within('.categories') do
    expect(current_scope).to have_content name
  end
end

When('I create a {string} category') do |name|
  step 'I access the new category form'
  step "I fill the category form for a \"#{name}\" category"
end

Given('I have a {string} category') do |name|
  group = @current_user.groups.first

  FactoryBot.create :category, group: group, user: @current_user, name: name
end

Given('I access the categories list') do
  step 'I click on "Categories" in the main menu'
end

When('I destroy the category named {string}') do |name|
  within('.category', text: name) do
    click_on 'Show more actions'
    click_on 'Destroy'
  end
end

Then("I don't see the {string} category in the list") do |name|
  within('.categories') do
    expect(current_scope).not_to have_content name
  end
end

Given('I have {int} categories in the current group') do |amount|
  group = @current_user.groups.first

  FactoryBot.create_list :category, amount, group: group, user: @current_user
end

Then('I see {int} categories') do |amount|
  within('.categories') do
    current_scope.assert_selector('.category', count: amount)
  end
end

When('I edit the {string} category') do |name|
  within('.category', text: name) do
    click_on 'Edit'
  end

  expect(page).to have_content 'Category edition'
end

When('I change the category name to {string}') do |name|
  step "I fill the category form for a \"#{name}\" category"
end

Given('I have no category in the current group') do
  group = @current_user.groups.first
  group.categories.destroy_all
end

Then('I see no category in the list') do
  within('.categories') do
    current_scope.assert_selector('.category', count: 0)

    expect(current_scope).to have_content 'You have no category for now'
  end
end

Then(/^the category form is empty(?: again)?$/) do
  expect(page.find_field('Name').value).to eq ''

  click_on 'Cancel'
end

Then('the category form is filled with the {string} data') do |name|
  expect(page.find_field('Name').value).to eq name
end

Then('I have the ability to create a new category from the form') do
  within('form[aria-label="Item edition form"]') do
    find('label', text: 'Category').click
    click_on 'Create new'
    expect(page).to have_content('New category')
  end
end
