When('I access my information page') do
  step 'I click on "Profile" in the main menu'
end

Then('I see my personal infos') do
  expect(page).to have_content @current_user.email
  expect(page).to have_content @current_user.name
end

Given('I access the personal information form') do
  step 'I access my information page'

  click_on 'Edit'
end

When('I change my email address to {string}') do |email|
  fill_in 'Email', with: email

  click_on 'Save'
end

When('I visit the link in the mail to confirm {string}') do |email|
  user = User.find_by unconfirmed_email: email

  visit user_confirmation_path params: { confirmation_token: user.confirmation_token }
end

When('I change password to {string}') do |password|
  fill_in 'New password', with: password
  fill_in 'Confirmation', with: password

  click_on 'Save'
end
