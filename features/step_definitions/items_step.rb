Given('I access the new item form') do
  within('header.toolbar') do
    click_on 'Add'
  end

  expect(page).to have_content('New item')
end

Given('I fill the item form for a {string} item') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see {string} in the items list') do |name|
  within('.items') do
    expect(current_scope).to have_content name
  end
end

When('I create a {string} item') do |name|
  step 'I access the new item form'
  step "I fill the item form for a \"#{name}\" item"
end

Given('I have a {string} item with a minimum quantity of {int}') do |name, min|
  group = @current_user.groups.first

  FactoryBot.create :item, group: group, user: @current_user, name: name, alert_threshold: min
end

Given('I have a {string} item') do |name|
  step "I have a \"#{name}\" item with a minimum quantity of 0"
end

Given('I access the items list') do
  step 'I click on "Items" in the main menu'
end

When('I destroy the item named {string}') do |name|
  within('.item', text: name) do
    click_on 'Show more actions'
    click_on 'Destroy'
  end
end

Then("I don't see the {string} item in the list") do |name|
  within('.items') do
    expect(current_scope).not_to have_content name
  end
end

Given('I have {int} items in the current group') do |amount|
  group = @current_user.groups.first

  FactoryBot.create_list :item, amount, group: group, user: @current_user
end

Then('I see {int} items') do |amount|
  within('.items') do
    current_scope.assert_selector('.item', count: amount)
  end
end

When('I edit the {string} item') do |name|
  within('.item', text: name) do
    click_on 'Edit'
  end

  expect(page).to have_content 'Item edition'
end

When('I change the item name to {string}') do |name|
  step "I fill the item form for a \"#{name}\" item"
end

Given('I have no item in the current group') do
  group = @current_user.groups.first
  group.items.destroy_all
end

Then('I see no item in the list') do
  within('.items') do
    current_scope.assert_selector('.item', count: 0)

    expect(current_scope).to have_content 'You have no item for now'
  end
end

Then(/^the item form is empty(?: again)?$/) do
  expect(page.find_field('Name').value).to eq ''

  click_on 'Cancel'
end

Then('the item form is filled with the {string} data') do |name|
  expect(page.find_field('Name').value).to eq name
end

Then('I see a warning concerning the {string} item') do |item|
  current_scope.assert_selector('.item h2.danger', text: item, count: 1)
end

When('I add the {string} to {string}') do |item, shopping_list|
  within('.item', text: item) do
    click_on 'Add to shopping list'
    click_on shopping_list
  end
end

Then('I have the ability to create a new item from the form') do
  find('label', text: 'Item').click
  click_on 'Create new'
  expect(page).to have_content('New item')
end

When('I open the sub-form to create an item') do
  step 'I have the ability to create a new item from the form'
end

When('I open the form to create a new item') do
  click_on 'Add'
end

When('I open the form to add a new item') do
  click_on 'Add item'
end
