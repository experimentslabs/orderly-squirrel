When(/^open main menu$/) do
  within('nav.navbar') do
    click_on 'Menu'
  end
end

When(/^close main menu$/) do
  click_on '×'
end

Given('I click on {string} in the main menu') do |element|
  within('nav.navbar') do
    click_on 'Menu'
    click_on element
  end
end

When('I click on {string}') do |string|
  click_on string
end
