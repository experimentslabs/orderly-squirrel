Given('I access the new shopping list form') do
  within('header.toolbar') do
    click_on 'Add'
  end

  expect(page).to have_content('New shopping list')
end

Given('I fill the shopping list form for a {string} shopping list') do |name|
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I see {string} in the shopping lists') do |name|
  within('.shopping-lists') do
    expect(current_scope).to have_content name
  end
end

When('I create a {string} shopping list') do |name|
  step 'I access the new shopping list form'
  step "I fill the shopping list form for a \"#{name}\" shopping list"
end

Given('I have a {string} shopping list') do |name|
  group = @current_user.groups.first

  FactoryBot.create :shopping_list, group: group, user: @current_user, name: name
end

Given('I access the shopping lists') do
  step 'I click on "Shopping lists" in the main menu'
end

When('I destroy the shopping list named {string}') do |name|
  within('.shopping-list', text: name) do
    click_on 'Show more actions'
    click_on 'Destroy'
  end
end

Then("I don't see the {string} shopping list in the list") do |name|
  within('.shopping-lists') do
    expect(current_scope).not_to have_content name
  end
end

Given('I have {int} shopping lists in the current group') do |amount|
  group = @current_user.groups.first

  FactoryBot.create_list :shopping_list, amount, group: group, user: @current_user
end

Then('I see {int} shopping lists') do |amount|
  within('.shopping-lists') do
    current_scope.assert_selector('.shopping-list', count: amount)
  end
end

When('I edit the {string} shopping list') do |name|
  within('.shopping-list', text: name) do
    click_on 'Edit'
  end

  expect(page).to have_content 'Editing a shopping list'
end

When('I change the shopping list name to {string}') do |name|
  step "I fill the shopping list form for a \"#{name}\" shopping list"
end

Given('I have no shopping list in the current group') do
  group = @current_user.groups.first
  group.shopping_lists.destroy_all
end

Then('I see no shopping list in the list') do
  within('.shopping-lists') do
    current_scope.assert_selector('.shopping_list', count: 0)

    expect(current_scope).to have_content 'You have no shopping list for now'
  end
end

Given('I have a {string} shopping list with the following items') do |name, table|
  data = table.raw

  shopping_list = FactoryBot.create :shopping_list, name: name, group: @current_user.groups.first

  data.each do |row|
    item = FactoryBot.create :item, name: row[0], group: @current_user.groups.first
    done = row[2] == 'done'
    FactoryBot.create :shopping_lists_item, item: item, shopping_list: shopping_list, quantity: row[1], done: done
  end
end

Then('I see a shopping list with {int} items and {int} done') do |items, done|
  expect(find('.shopping-list', text: "#{done}/#{items}"))
end

When('I access the {string} shopping list') do |name|
  step 'I access the shopping lists'
  click_on name
end

When('I mark the the {string} item as done') do |name|
  within('.shopping-lists-item', text: name) do
    click_on 'Mark as done'
  end
end

Then('The {string} item is marked as done') do |name|
  page.assert_selector('.shopping-lists-item.shopping-lists-item--done', text: name, count: 1)
end

When('I mark the the {string} item as undone') do |name|
  within('.shopping-lists-item', text: name) do
    click_on 'Mark as todo'
  end
end

Then('The {string} item is marked as undone') do |name|
  page.assert_selector('.shopping-lists-item:not(.shopping-lists-item--done)', text: name, count: 1)
end

Then('I see the {string} shopping list details') do |name|
  within('.toolbar__title') do
    expect(current_scope).to have_content name
  end
end

When(/^I add (\d+|the) "(.+)" in the shopping list$/) do |amount, item|
  quantity = amount == 'the' ? 1 : amount.to_i

  step 'I open the form to add an item in the shopping list'

  fill_in 'Item', with: item
  click_on item

  fill_in 'Quantity', with: quantity

  click_on 'Save'
end

Then('I see the {string} item in the shopping list') do |item|
  within('.shopping-list') do
    expect(current_scope).to have_content item
  end
end

Given(/^I have a "(.+)" shopping list with (\d+|some) "(.+)" in it$/) do |shopping_list, amount, item|
  quantity      = amount == 'some' ? 1 : amount.to_i
  shopping_list = FactoryBot.create :shopping_list, name: shopping_list, group: @current_user.groups.first
  item          = FactoryBot.create :item, name: item, group: @current_user.groups.first
  FactoryBot.create :shopping_lists_item, shopping_list: shopping_list, item: item, quantity: quantity
end

When('I remove the {string} from the shopping list') do |item|
  within('.shopping-list .shopping-lists-item', text: item) do
    click_on 'Show more actions'
    click_on 'Remove'
  end
end

Then("I don't see the {string} item in the shopping list") do |item|
  within('.shopping-list') do
    expect(current_scope).not_to have_content item
  end
end

Then('I see {int} {string} items in the shopping list') do |amount, item|
  within('.shopping-list') do
    within('.shopping-lists-item', text: item) do
      expect(current_scope).to have_content amount
    end
  end
end

When(/^I (add|remove) (\d+) to "(.+)" (?:in|from) the shopping list$/) do |action, amount, item|
  button_string = action == 'add' ? '+' : '-'

  within('.shopping-list') do
    within('.shopping-lists-item', text: item) do
      click_on 'Show more actions'
      amount.times { click_on button_string }
    end
  end
end

Then('I mark the shopping list as done') do
  click_on "I'm done!"
end

Then('the shopping list {string} is marked as done') do |name|
  page.assert_selector('.shopping-list.shopping-list--done', text: name, count: 1)
end

When('I open the form to add an item in the shopping list') do
  click_on 'Add item'
end

Then(/^the shopping_list-item form for "(.+)" is empty(?: again)?$/) do |shopping_list|
  expect(page.find_field('Shopping list', disabled: true).value).to eq shopping_list
  expect(page.find_field('Item').value).to eq ''
  expect(page.find_field('Quantity').value).to eq '1'
  click_on 'Cancel'
end

When('I open the form to edit the {string} in the {string} shopping list') do |item, shopping_list|
  within('.shopping-list', text: shopping_list) do
    within('.shopping-lists-item', text: item) do
      click_on 'Show more actions'
      click_on 'Edit'
    end
  end
end

Then('the shopping_list-item form is filled with the {string} and {string} data') do |shopping_list, item|
  db_item          = Item.find_by name: item
  db_shopping_list = ShoppingList.find_by name: shopping_list
  association      = ShoppingListsItem.find_by shopping_list: db_shopping_list, item: db_item

  expect(page.find_field('Shopping list').value).to eq shopping_list
  page.assert_selector('label', text: 'Item', count: 0)
  expect(page.find_field('Quantity').value).to eq association.quantity.to_s

  click_on 'Cancel'
end

Then(/^the shopping list form is empty(?: again)?$/) do
  expect(page.find_field('Name').value).to eq ''

  click_on 'Cancel'
end

Then('the shopping list form is filled with the {string} data') do |name|
  expect(page.find_field('Name').value).to eq name
end

When('I assign {string} to to pick the {string}') do |user, item|
  within('.shopping-lists-item', text: item) do
    click_on 'Assign someone'

    click_on user
  end
end

Then('I see that {string} is assigned to pick the {string}') do |_user, item|
  within('.shopping-lists-item', text: item) do
    find('button[title="Change assignee"]')
  end
end
