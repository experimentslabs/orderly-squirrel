Feature: User account
  As an user
  In order to keep my infos up to date
  I want to be able to change my infos

  Background:
    Given I'm authenticated as an user with email "me@example.com"

  Scenario: Display my information
    When I access my information page
    Then I see my personal infos

  Scenario: Email change
    Given I access the personal information form
    When I change my email address to "me@home.com"
    And I sign out
    And I visit the link in the mail to confirm "me@home.com"
    And I authenticate as "me@home.com"
    Then I see the application

  Scenario: Password change
    Given I access the personal information form
    When I change password to "new password"
    And I sign out
    And I authenticate as "me@example.com" with password "new password"
    Then I see the application
