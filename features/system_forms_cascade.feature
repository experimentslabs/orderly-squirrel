Feature: System: Forms cascading
  As an user
  In order to quickly create content
  I want to be able to create elements from other elements forms

  Background:
    Given I have an active account for "jane@doe.com"
    And I have a "Fridge" container
    And I have a "Butter" item
    And I have a "Next groceries" shopping list
    And I authenticate as "jane@doe.com"

  Scenario: containers > add item > new item
    Given I access the containers list
    When I open the form to add an item in the "Fridge"
    Then I have the ability to create a new item from the form

  Scenario: containers > add item > new item > new category
    Given I access the containers list
    And I open the form to add an item in the "Fridge"
    When I open the sub-form to create an item
    Then I have the ability to create a new category from the form

  Scenario: items > new > new category
    Given I access the items list
    When I open the form to create a new item
    Then I have the ability to create a new category from the form

  Scenario: shopping list > add item > new item
    Given I access the "Next groceries" shopping list
    When I open the form to add a new item
    Then I have the ability to create a new item from the form

  Scenario: shopping list > add item > new item > new category
    Given I access the "Next groceries" shopping list
    When I open the form to add a new item
    When I open the sub-form to create an item
    Then I have the ability to create a new category from the form
