# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Rails.logger.debug "Seeding for #{Rails.env}"
require File.join(__dir__, 'seeds_production') if Rails.env.production?
require File.join(__dir__, 'seeds_development') if Rails.env.development?
