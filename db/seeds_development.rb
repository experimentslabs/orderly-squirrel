# A default group is created when user is created.
user  = FactoryBot.create :user_known
group = user.groups.first

FactoryBot.create_list :user, 5

second_user = User.second
second_user.groups.push(group)

category = FactoryBot.create :category, group: group
FactoryBot.create_list :container, 2, group: group
FactoryBot.create_list :item, 5, group: group, category: category

Item.all.each { |item| Container.first.items.push item }
