class RemoveDoneFromShoppingLists < ActiveRecord::Migration[5.2]
  def change
    remove_column :shopping_lists, :done, :boolean, null: false, default: false
  end
end
