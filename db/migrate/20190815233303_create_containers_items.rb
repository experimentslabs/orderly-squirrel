class CreateContainersItems < ActiveRecord::Migration[5.2]
  def change
    create_table :containers_items do |t|
      t.references :container
      t.references :item
      t.integer :quantity, null: false, default: 0

      t.timestamps

      t.index [:container_id, :item_id], unique: true
    end
  end
end
