class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name, null: false

      t.references :creator, foreign_key: { to_table: :users }

      t.timestamps
    end

    create_table :groups_users do |t|
      t.references :group, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :groups_users, [:group_id, :user_id], unique: true
  end
end
