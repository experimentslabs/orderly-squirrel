class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name, null: false, default: nil

      t.references :category, foreign_key: true
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
