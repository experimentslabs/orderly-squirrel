class AddThresholdToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :alert_threshold, :integer, null: false, default: 0
  end
end
