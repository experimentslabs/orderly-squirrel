class CreateShoppingLists < ActiveRecord::Migration[5.2]
  def change
    create_table :shopping_lists do |t|
      t.string   :name, null: false, default: nil
      t.boolean  :done, null: false, default: false
      t.datetime :done_at
      t.boolean  :template, null: false, default: false
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
