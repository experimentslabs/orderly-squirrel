class CreateShoppingListsItems < ActiveRecord::Migration[5.2]
  def change
    create_table :shopping_lists_items do |t|
      t.references :shopping_list
      t.references :item
      t.integer :quantity, null: false, default: 0
      t.boolean :done, null: false, default: false

      t.timestamps

      t.index [:shopping_list_id, :item_id], unique: true
    end
  end
end
