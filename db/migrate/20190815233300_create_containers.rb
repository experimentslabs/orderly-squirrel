class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :name, null: false, default: nil

      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
