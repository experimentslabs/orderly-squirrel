class AddUserToShoppingListItems < ActiveRecord::Migration[5.2]
  def change
    change_table :shopping_lists_items do |t|
      t.references :assignee, index: true, foreign_key: { to_table: :users }
    end
  end
end
