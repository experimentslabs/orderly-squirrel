class AddDeletedAtToModels < ActiveRecord::Migration[5.2]
  def change
    add_column :categories,           :deleted_at, :datetime
    add_column :containers,           :deleted_at, :datetime
    add_column :containers_items,     :deleted_at, :datetime
    add_column :groups,               :deleted_at, :datetime
    add_column :groups_users,         :deleted_at, :datetime
    add_column :items,                :deleted_at, :datetime
    add_column :logs,                 :deleted_at, :datetime
    add_column :shopping_lists,       :deleted_at, :datetime
    add_column :shopping_lists_items, :deleted_at, :datetime
    add_column :users,                :deleted_at, :datetime

    add_index :categories,           :deleted_at
    add_index :containers,           :deleted_at
    add_index :containers_items,     :deleted_at
    add_index :groups,               :deleted_at
    add_index :groups_users,         :deleted_at
    add_index :items,                :deleted_at
    add_index :logs,                 :deleted_at
    add_index :shopping_lists,       :deleted_at
    add_index :shopping_lists_items, :deleted_at
    add_index :users,                :deleted_at
  end
end
