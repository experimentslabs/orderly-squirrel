# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_06_082720) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_categories_on_deleted_at"
    t.index ["group_id"], name: "index_categories_on_group_id"
  end

  create_table "containers", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_containers_on_deleted_at"
    t.index ["group_id"], name: "index_containers_on_group_id"
  end

  create_table "containers_items", force: :cascade do |t|
    t.bigint "container_id"
    t.bigint "item_id"
    t.integer "quantity", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["container_id", "item_id"], name: "index_containers_items_on_container_id_and_item_id", unique: true
    t.index ["container_id"], name: "index_containers_items_on_container_id"
    t.index ["deleted_at"], name: "index_containers_items_on_deleted_at"
    t.index ["item_id"], name: "index_containers_items_on_item_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "creator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["creator_id"], name: "index_groups_on_creator_id"
    t.index ["deleted_at"], name: "index_groups_on_deleted_at"
  end

  create_table "groups_users", force: :cascade do |t|
    t.bigint "group_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_groups_users_on_deleted_at"
    t.index ["group_id", "user_id"], name: "index_groups_users_on_group_id_and_user_id", unique: true
    t.index ["group_id"], name: "index_groups_users_on_group_id"
    t.index ["user_id"], name: "index_groups_users_on_user_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "category_id"
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "alert_threshold", default: 0, null: false
    t.index ["category_id"], name: "index_items_on_category_id"
    t.index ["deleted_at"], name: "index_items_on_deleted_at"
    t.index ["group_id"], name: "index_items_on_group_id"
  end

  create_table "logs", force: :cascade do |t|
    t.string "action", null: false
    t.integer "loggable_id"
    t.string "loggable_type"
    t.bigint "user_id"
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_logs_on_deleted_at"
    t.index ["group_id"], name: "index_logs_on_group_id"
    t.index ["user_id"], name: "index_logs_on_user_id"
  end

  create_table "shopping_lists", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "done_at"
    t.boolean "template", default: false, null: false
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_shopping_lists_on_deleted_at"
    t.index ["group_id"], name: "index_shopping_lists_on_group_id"
  end

  create_table "shopping_lists_items", force: :cascade do |t|
    t.bigint "shopping_list_id"
    t.bigint "item_id"
    t.integer "quantity", default: 0, null: false
    t.boolean "done", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.bigint "assignee_id"
    t.index ["assignee_id"], name: "index_shopping_lists_items_on_assignee_id"
    t.index ["deleted_at"], name: "index_shopping_lists_items_on_deleted_at"
    t.index ["item_id"], name: "index_shopping_lists_items_on_item_id"
    t.index ["shopping_list_id", "item_id"], name: "index_shopping_lists_items_on_shopping_list_id_and_item_id", unique: true
    t.index ["shopping_list_id"], name: "index_shopping_lists_items_on_shopping_list_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "categories", "groups"
  add_foreign_key "containers", "groups"
  add_foreign_key "groups", "users", column: "creator_id"
  add_foreign_key "groups_users", "groups"
  add_foreign_key "groups_users", "users"
  add_foreign_key "items", "categories"
  add_foreign_key "items", "groups"
  add_foreign_key "logs", "groups"
  add_foreign_key "logs", "users"
  add_foreign_key "shopping_lists", "groups"
  add_foreign_key "shopping_lists_items", "users", column: "assignee_id"
end
