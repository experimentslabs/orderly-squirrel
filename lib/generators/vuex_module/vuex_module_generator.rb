class VuexModuleGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)

  def create_dir_structure
    FileUtils.mkdir_p 'app/javascript/store/modules'
    template 'module.js.erb', "app/javascript/store/modules/#{name}.js"
  end
end
