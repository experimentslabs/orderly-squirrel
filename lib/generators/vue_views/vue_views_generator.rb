class VueViewsGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)

  def create_dir_structure
    views_dir = "app/javascript/templates/#{name}"
    FileUtils.mkdir_p views_dir
    template 'form.vue.erb', "#{views_dir}/_Form.vue"
    template 'item.vue.erb', "#{views_dir}/_#{name.classify}.vue"
    template 'index.vue.erb', "#{views_dir}/Index.vue"
    template 'show.vue.erb', "#{views_dir}/Show.vue"

    summary(name)
  end

  private

  def summary(name) # rubocop:disable Metrics/AbcSize
    model = name.classify.constantize
    fields = []
    model.attribute_names.each do |attribute|
      fields.push "      #{attribute}: '#{attribute.humanize}',"
    end
    say <<~TXT

      You should add the following routes to routes.js:

        {
          path: '/#{name}',
          name: '#{name}',
          component: () => import(/* webpackChunkName: "#{name}" */ './templates/#{name}/Index'),
        },
        {
          path: '/#{name}/:id',
          name: '#{name.singularize}',
          component: () => import(/* webpackChunkName: "#{name.singularize}" */ './templates/#{name}/Show'),
        },

      And the following locales:

        #{name}: {
          fields: {
      #{fields.join("\n")}
          },
          form: {
            aria_label: '#{name.singularize.humanize} edition form',
            errors: 'Errors prevented to save this #{name.singularize}.',
            edition: '#{name.singularize.humanize} edition',
            new: 'New #{name.singularize.humanize.downcase}',
          },
          index: {
            title: '#{name.humanize}',
            no_content: 'You have no #{name.singularize.humanize.downcase} for now; you should start by creating one.',
            create_button: 'Create a #{name.singularize} !',
          },
        },

      Don't forget to lint the generated files :

        yarn fix:js

    TXT
  end
end
