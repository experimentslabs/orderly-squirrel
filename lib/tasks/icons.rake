require 'nokogiri'

namespace :icons do # rubocop:disable Metrics/BlockLength
  task build: :environment do
    CONFIGURATION_FILE = Rails.root.join('app', 'javascript', 'icons.yaml')
    raise 'Missing "icons.yaml" file in "app/javascripts"' unless File.exist? CONFIGURATION_FILE

    CONFIG    = YAML.load_file CONFIGURATION_FILE
    BASE_PATH = lib_base_path

    SVG_STRING   = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  width="0" height="0" style="display:none;"/>'.freeze
    SVG_VIEW_BOX = '0 0 24 24'.freeze

    # Prepare definition file content
    svg_definitions    = Nokogiri::XML.fragment SVG_STRING
    xml_namespace      = svg_definitions.children.first.namespace.href
    defs_node          = svg_definitions.xpath('./ns:svg', ns: xml_namespace).first
    defs_node['class'] = 'icons-definition'

    CONFIG['icons'].each do |name, path|
      puts "Processing #{name}"
      complete_path = File.join(BASE_PATH, path)
      next unless complete_path_exists? complete_path

      svg = Nokogiri::XML.fragment File.read(complete_path)

      svg_groups = svg.xpath('./ns:svg/ns:g', ns: xml_namespace)
      next unless valid_groups_amount svg_groups, path

      # Add definition
      defs_node.add_child create_icon_def(name, svg_groups.first)
    end

    File.write(Rails.root.join(CONFIG['output']), svg_definitions)
  end

  def complete_path_exists?(complete_path)
    return true if File.exist? complete_path

    puts "Missing file #{complete_path}, skipping"
    false
  end

  def valid_groups_amount(svg_groups, path)
    amount = svg_groups.count
    return true if amount == 1

    puts "Found #{amount} group(s) in #{path}. Ignoring :/"
    false
  end

  def lib_base_path
    path = CONFIG['base']
    raise "Missing icon library in #{path}" unless File.exist? path

    path
  end

  def icon_name(icon)
    icon_id(icon).underscore.camelize
  end

  def icon_id(icon)
    "#{CONFIG['id_prefix']}#{icon}"
  end

  def create_icon_def(name, svg_group)
    id       = icon_id name
    fragment = Nokogiri::XML.fragment "<symbol id='#{id}' viewBox='#{SVG_VIEW_BOX}'/>"
    group    = svg_group
    fragment.xpath('./symbol').first.add_child group
    fragment
  end
end
