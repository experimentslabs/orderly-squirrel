module Context
  class User
    attr_reader :user, :group_id

    def initialize(user, group_id)
      @user     = user
      @group_id = group_id
    end
  end
end
