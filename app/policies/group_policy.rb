class GroupPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    check_group
  end

  def create?
    check_group
  end

  def update?
    check_group
  end

  def destroy?
    check_group
  end

  def invite?
    check_group
  end

  def leave?
    check_group
  end

  def remove?
    check_group
  end

  private

  def check_group
    context.user.groups.pluck(:id).include? @record.id
  end
end
