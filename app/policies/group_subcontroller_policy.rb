class GroupSubcontrollerPolicy < ApplicationPolicy
  def index?
    check_group
  end

  def show?
    check_group
  end

  def create?
    check_group
  end

  def update?
    check_group
  end

  def destroy?
    check_group
  end

  private

  def check_group
    context.user.groups.pluck(:id).include? context.group_id.to_i
  end
end
