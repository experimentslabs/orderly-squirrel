class UserPolicy < ApplicationPolicy
  def show?
    record.present?
  end
end
