class ContainersItemPolicy < GroupSubcontrollerPolicy
  # ContainersItem don't have group_id to check on.
  # The group policy should be checked in the controller
  # and the entity should be filtered in the controller
  # with `model_scope`
  def show?
    true
  end
end
