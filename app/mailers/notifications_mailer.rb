class NotificationsMailer < ApplicationMailer
  def assigned_to_item
    @shopping_lists_item = params[:shopping_lists_item]
    @user                = @shopping_lists_item.assignee
    @item                = @shopping_lists_item.item
    @group               = @item.group
    @shopping_list       = @shopping_lists_item.shopping_list

    mail to: @user.email, subject: "[OrderlySquirrel] You have been assigned to pick some #{@item.name}"
  end
end
