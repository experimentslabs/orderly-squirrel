class Item < ApplicationRecord
  acts_as_paranoid

  include Loggable

  validates :name, presence: true
  validates :alert_threshold, numericality: { minimum: 0 }
  validates_with GroupableValidator, if: :user
  validates_with SameGroupValidator, relation: :category

  belongs_to :category, optional: true
  belongs_to :group
  has_many :containers_items # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :containers, through: :containers_items, dependent: :destroy
  has_many :shopping_lists_items # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :shopping_lists, through: :shopping_lists_items, dependent: :destroy

  scope :in_group, ->(group_id) { where(group_id: group_id) }

  def amount_stashed
    containers_items.sum(:quantity)
  end
end
