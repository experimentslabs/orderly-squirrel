class ContainersItem < ApplicationRecord
  acts_as_paranoid

  validates :quantity, numericality: { greater_than_or_equal_to: 0 }
  validates :item_id, uniqueness: { scope: :container_id }
  validates :container, presence: true
  validates :item, presence: true
  validates_with SameGroupJoinValidator, first: :container, second: :item

  belongs_to :container
  belongs_to :item

  scope :in_group, ->(group_id) { includes(:container).where(containers: { group_id: group_id }) }
  scope :in_group_container, ->(group_id, container_id) { in_group(group_id).where(container_id: container_id) }
end
