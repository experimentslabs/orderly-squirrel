class ShoppingListsItem < ApplicationRecord
  acts_as_paranoid

  validates :quantity, numericality: { greater_than_or_equal_to: 1 }
  validates :item_id, uniqueness: { scope: :shopping_list_id }
  validates :shopping_list, presence: true
  validates :item, presence: true
  validates_with SameGroupJoinValidator, first: :shopping_list, second: :item

  belongs_to :item
  belongs_to :shopping_list
  belongs_to :assignee, class_name: 'User', optional: true

  scope :assigned_to, ->(user_id) { where(assignee_id: user_id) }
  scope :in_group, ->(group_id) { includes(:shopping_list).where(shopping_lists: { group_id: group_id }) }
  scope :in_group_container, ->(group_id, shopping_list_id) { in_group(group_id).where(shopping_list_id: shopping_list_id) }

  before_validation :determine_quantity
  before_save :prepare_notifications
  after_save :send_notifications

  private

  def determine_quantity
    # Ignore updates and entities with missing item (validation will catch these)
    return unless new_record? && item

    self.quantity = item.alert_threshold - item.amount_stashed if quantity.zero?
    self.quantity = 1 unless quantity >= 1
  end

  def prepare_notifications
    @notify_for = []
    @notify_for.push(:item_assigned) if changes.include?(:assignee_id) && !assignee_id.nil?
  end

  def send_notifications
    NotificationsMailer.with(shopping_lists_item: self).assigned_to_item.deliver_later if @notify_for.include? :item_assigned
  end
end
