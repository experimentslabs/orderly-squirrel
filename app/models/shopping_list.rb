class ShoppingList < ApplicationRecord
  attr_accessor :done

  acts_as_paranoid

  include Loggable

  validates :name, presence: true
  validates :done_at, presence: true, if: :done
  validates :done_at, absence: true, unless: :done
  validates_with GroupableValidator, if: :user

  belongs_to :group
  has_many :logs, as: :loggable, dependent: :destroy
  has_many :shopping_lists_items # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :items, through: :shopping_lists_items, dependent: :destroy

  scope :in_group, ->(group_id) { where(group_id: group_id) }

  before_validation :fill_end_date

  private

  def fill_end_date
    self.done_at = Time.zone.now if done && done_at.nil?
  end
end
