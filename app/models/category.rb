class Category < ApplicationRecord
  acts_as_paranoid

  include Loggable

  validates :name, presence: true
  validates_with GroupableValidator, if: :user

  belongs_to :group
  has_many :items, dependent: :nullify

  scope :in_group, ->(group_id) { where(group_id: group_id) }
end
