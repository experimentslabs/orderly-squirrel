module Loggable
  extend ActiveSupport::Concern
  included do
    attr_accessor :user

    has_many :logs, as: :loggable, dependent: :delete_all

    after_commit :log_action, if: :user
  end

  private

  def log_action
    action = :update
    action = :create if transaction_include_any_action?([:create])
    action = :destroy if transaction_include_any_action?([:destroy])

    Log.create! loggable: self, action: action, user: @user, group_id: group_id
  end
end
