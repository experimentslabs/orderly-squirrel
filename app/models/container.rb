class Container < ApplicationRecord
  acts_as_paranoid

  include Loggable

  validates :name, presence: true
  validates_with GroupableValidator, if: :user

  belongs_to :group
  has_many :containers_items # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :items, through: :containers_items, dependent: :destroy

  scope :in_group, ->(group_id) { where(group_id: group_id) }
end
