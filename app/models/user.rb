class User < ApplicationRecord
  acts_as_paranoid

  attr_accessor :current_group

  validates :name, presence: true
  validate :avatar_format

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  has_many :created_groups, class_name: 'Group', foreign_key: :creator_id, inverse_of: :creator, dependent: :nullify
  has_many :groups_users # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :groups, through: :groups_users, dependent: :destroy
  has_many :logs, dependent: :destroy
  has_many :shopping_lists_items, as: :assigned_items, foreign_key: :assignee_id, dependent: :destroy, inverse_of: :assignee

  has_one_attached :avatar

  scope :known_users, ->(user_id) { find(user_id).known_users }

  after_create :create_default_group

  def know?(id)
    known_user(id)
  end

  def known_user(id)
    known_users.find_by(id: id)
  end

  def known_users
    User.includes(:groups).where(groups: { id: group_ids })
  end

  def leave_group(group)
    raise ForbiddenAction, I18n.t('user.errors.cant_leave_group') if group.creator_id == id

    groups_users.find_by(user: self, group: group).really_destroy!
  end

  def in_group?(group_id)
    group_ids.include? group_id
  end

  # Use with url_for() in views
  def avatar_representation(size)
    return nil unless avatar.attached?

    avatar.representation(resize_to_fill: [size, size, { gravity: 'Center' }]).processed
  end

  private

  def create_default_group
    Group.create! name: I18n.t('group.default_group_name', username: name), creator: self, users: [self]
  end

  def avatar_format
    return unless avatar.attached?

    errors.add(:avatar, I18n.t('model.user.needs_to_be_a_png_or_jpg_image')) unless %w[image/png image/jpeg].include? avatar.blob.content_type
  end
end
