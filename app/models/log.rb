class Log < ApplicationRecord
  validates :action, presence: true

  belongs_to :user
  belongs_to :group
  belongs_to :loggable, polymorphic: true

  scope :in_group, ->(group_id) { where(group_id: group_id) }
end
