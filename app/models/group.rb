class Group < ApplicationRecord
  acts_as_paranoid

  validates :name, presence: true

  belongs_to :creator, class_name: 'User', inverse_of: :created_groups
  has_many :groups_users # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :users, through: :groups_users, dependent: :destroy
  has_many :containers, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :shopping_lists, dependent: :destroy
  has_many :logs, dependent: :delete_all

  # Returns a group if it was created by given person
  scope :find_created_by, ->(creator_id, group_id) { find_by creator_id: creator_id, id: group_id }
  # Returns a group if it a given person is in it
  scope :find_with_user, ->(user_id, group_id) { where(id: group_id).includes(:users).find_by(users: { id: user_id }) }

  after_create :add_creator

  def add_user(email)
    user = User.find_by email: email

    unless user
      errors.add(:user, I18n.t('group.error.invalid_user'))
      return false
    end

    users.push user
  end

  def remove_user(user_id)
    raise ForbiddenAction, I18n.t('group.error.cant_remove_creator') if user_id.to_i == creator_id

    groups_users.find_by!(user_id: user_id, group_id: id).really_destroy!
  end

  private

  def add_creator
    users.push creator unless user_ids.include? creator_id
  end
end
