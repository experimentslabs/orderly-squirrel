class SameGroupValidator < ActiveModel::Validator
  def validate(record)
    relation = record.send(options[:relation])
    return true if relation.nil?

    record.errors[record.class.name] << I18n.t('validators.same_group.not_same_group', relation: options[:relation]) if relation.group_id != record.group_id
  end
end
