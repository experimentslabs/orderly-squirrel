class GroupableValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:group] << I18n.t('validators.groupable.not_joined') unless record.user&.in_group?(record.group_id)
  end
end
