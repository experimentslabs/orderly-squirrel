class SameGroupJoinValidator < ActiveModel::Validator
  def validate(record)
    first_relation  = record.send(options[:first])
    second_relation = record.send(options[:second])

    return unless first_relation && second_relation

    record.errors[:groups] << I18n.t('validators.same_group_join.not_same_group', first: options[:first], second: options[:second]) if in_different_groups first_relation, second_relation
  end

  private

  def in_different_groups(first_relation, second_relation)
    first_relation.group_id != second_relation.group_id
  end
end
