module Api
  class CategoriesController < ApiGroupSubcontroller
    before_action :set_and_authorize_category, only: [:show, :update, :destroy]

    def index
      @categories = model_scope
    end

    def show; end

    def create
      @category = Category.new(category_params)
      @category.group = @group

      if @category.save
        render :show, status: :created, location: api_group_category_url(@category.group_id, @category)
      else
        render json: { errors: @category.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @category.update(category_params)
        render :show, status: :ok, location: api_group_category_url(@category.group_id, @category.id)
      else
        render json: { errors: @category.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @category.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_category
      @category = model_scope.find params[:id]
      authorize @category

      @category.user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name)
    end

    def model_scope
      Category.in_group params[:group_id]
    end
  end
end
