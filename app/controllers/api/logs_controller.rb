module Api
  class LogsController < ApiGroupSubcontroller
    before_action :set_and_authorize_log, only: [:show]

    def index
      @logs = model_scope
    end

    def show; end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_log
      @log = model_scope.find(params[:id])
      authorize @log
    end

    def model_scope
      Log.in_group params[:group_id] # current_user.current_group.id
    end
  end
end
