module Api
  class UsersController < ApiController
    before_action :set_and_authorize_user, only: [:show]
    before_action :set_self_user, only: [:update, :me]

    def index
      @users = model_scope
    end

    def show
      render json: { error: t('.not_found', id: params[:id]) }, status: :not_found unless @user
    end

    def me
      render :show
    end

    def update
      if @user.update(current_user_params)
        render :show, status: :ok, location: api_user_url(@user)
      else
        render json: { errors: @user.errors }, status: :unprocessable_entity
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_user
      @user = model_scope.find(params[:id])
      authorize @user
    end

    def set_self_user
      @user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def current_user_params
      params.require(:user).permit(:name, :email, :password, :avatar)
    end

    def model_scope
      current_user.known_users
    end
  end
end
