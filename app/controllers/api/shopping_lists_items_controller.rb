module Api
  class ShoppingListsItemsController < ApiGroupSubcontroller
    before_action :set_and_authorize_shopping_lists_item, only: [:show, :update, :destroy]

    def index
      @shopping_lists_items = model_scope
    end

    def show; end

    def create
      @shopping_lists_item = ShoppingListsItem.new(shopping_lists_item_params_create)

      if @shopping_lists_item.save
        render :show, status: :created, location: api_group_shopping_lists_item_url(params[:group_id], @shopping_lists_item)
      else
        render json: { errors: @shopping_lists_item.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @shopping_lists_item.update(shopping_lists_item_params_update)
        render :show, status: :ok, location: api_group_shopping_lists_item_url(params[:group_id], @shopping_lists_item)
      else
        render json: { errors: @shopping_lists_item.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @shopping_lists_item.really_destroy!
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_shopping_lists_item
      @shopping_lists_item = model_scope.find(params[:id])
      authorize @shopping_lists_item
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shopping_lists_item_params_create
      params.require(:shopping_lists_item).permit(:shopping_list_id, :item_id, :assignee_id, :quantity)
    end

    def shopping_lists_item_params_update
      params.require(:shopping_lists_item).permit(:shopping_list_id, :assignee_id, :quantity, :done)
    end

    def model_scope
      ShoppingListsItem.in_group(params[:group_id])
    end
  end
end
