module Api
  class ContainersController < ApiGroupSubcontroller
    before_action :set_and_authorize_container, only: [:show, :update, :destroy]

    def index
      @containers = model_scope
    end

    def show; end

    def create
      @container = Container.new(container_params)
      @container.group = @group

      if @container.save
        render :show, status: :created, location: api_group_container_url(@container.group_id, @container)
      else
        render json: { errors: @container.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @container.update(container_params)
        render :show, status: :ok, location: api_group_container_url(@container.group_id, @container.id)
      else
        render json: { errors: @container.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @container.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_container
      @container = model_scope.find params[:id]
      authorize @container

      @container.user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def container_params
      params.require(:container).permit(:name)
    end

    def model_scope
      Container.in_group params[:group_id]
    end
  end
end
