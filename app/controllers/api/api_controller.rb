module Api
  class ApiController < ApplicationController
    # API behavior
    skip_before_action :verify_authenticity_token
    respond_to :json

    # Error handling
    rescue_from ActiveRecord::RecordNotFound, with: :error_resource_not_found
    rescue_from ActionController::ParameterMissing, with: :error_unacceptable
    rescue_from ForbiddenAction, with: :error_action_forbidden
    rescue_from Pundit::NotAuthorizedError, with: :error_action_forbidden

    private

    def error_resource_not_found(exception)
      message = exception.message || "Resource not found or you're not allowed to access it"
      render_error :not_found, message: message
    end

    def error_action_forbidden(exception)
      message = exception.message || "You're not allowed to perform this action"
      render_error :forbidden, message: message
    end

    def error_unacceptable(exception)
      message = exception.message || 'The data in incomplete or malformed'
      render_error :unprocessable_entity, message: message, many: true
    end

    def render_error(status, message: nil, many: false)
      key = many ? :errors : :error
      render json: { key => message }, status: status
    end
  end
end
