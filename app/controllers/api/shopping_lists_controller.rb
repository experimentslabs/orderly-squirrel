module Api
  class ShoppingListsController < ApiGroupSubcontroller
    before_action :set_and_authorize_shopping_list, only: [:show, :update, :destroy]

    def index
      @shopping_lists = model_scope
    end

    def show; end

    def create
      @shopping_list = ShoppingList.new(shopping_list_params_create)
      @shopping_list.group = @group

      if @shopping_list.save
        render :show, status: :created, location: api_group_shopping_list_url(@shopping_list.group_id, @shopping_list)
      else
        render json: { errors: @shopping_list.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @shopping_list.update(shopping_list_params_update)
        render :show, status: :ok, location: api_group_shopping_list_url(@shopping_list.group_id, @shopping_list)
      else
        render json: { errors: @shopping_list.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @shopping_list.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_shopping_list
      @shopping_list = model_scope.find(params[:id])
      authorize @shopping_list

      @shopping_list.user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shopping_list_params_create
      params.require(:shopping_list).permit(:name, :template)
    end

    def shopping_list_params_update
      params.require(:shopping_list).permit(:name, :done, :template)
    end

    def model_scope
      ShoppingList.in_group params[:group_id]
    end
  end
end
