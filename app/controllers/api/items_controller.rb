module Api
  class ItemsController < ApiGroupSubcontroller
    before_action :set_and_authorize_item, only: [:show, :update, :destroy]

    def index
      @items = model_scope
    end

    def show; end

    def create
      @item = Item.new(item_params)
      @item.group = @group

      if @item.save
        render :show, status: :created, location: api_group_item_url(@item.group_id, @item)
      else
        render json: { errors: @item.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @item.update(item_params)
        render :show, status: :ok, location: api_group_item_url(@item.group_id, @item)
      else
        render json: { errors: @item.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @item.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_item
      @item = model_scope.find(params[:id])
      authorize @item

      @item.user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :category_id, :alert_threshold)
    end

    def model_scope
      Item.in_group params[:group_id]
    end
  end
end
