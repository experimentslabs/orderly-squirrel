module Api
  class ApiGroupSubcontroller < ApiController
    before_action :set_group
    before_action :authorize_group

    private

    def authorize_group
      authorize @group, :show?
    end

    def set_group
      @group = Group.find(params[:group_id])
    end
  end
end
