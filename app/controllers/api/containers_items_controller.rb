module Api
  class ContainersItemsController < ApiGroupSubcontroller
    before_action :set_and_authorize_containers_item, only: [:show, :update, :destroy]

    def index
      @containers_items = model_scope
    end

    def show; end

    def create
      @containers_item = ContainersItem.new(containers_item_params_create)

      if @containers_item.save
        render :show, status: :created, location: api_group_containers_item_url(params[:group_id], @containers_item)
      else
        render json: { errors: @containers_item.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @containers_item.update(containers_item_params_update)
        render :show, status: :ok, location: api_group_containers_item_url(params[:group_id], @containers_item)
      else
        render json: { errors: @containers_item.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @containers_item.really_destroy!
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_and_authorize_containers_item
      @containers_item = model_scope.find(params[:id])
      authorize @containers_item
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def containers_item_params_create
      params.require(:containers_item).permit(:container_id, :item_id, :quantity)
    end

    def containers_item_params_update
      params.require(:containers_item).permit(:container_id, :quantity)
    end

    def model_scope
      ContainersItem.in_group(params[:group_id])
    end
  end
end
