module Api
  class GroupsController < ApiController
    before_action :set_and_authorize_group, only: [:show, :update, :destroy, :invite, :remove, :leave]
    before_action :check_creator, only: [:remove, :update, :destroy, :invite]

    def index
      authorize Group
      @groups = model_scope
    end

    def show; end

    def create
      @group = Group.new(group_params)

      if current_user.created_groups.push @group
        render :show, status: :created, location: api_group_url(@group)
      else
        render json: { errors: @group.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @group.update(group_params)
        render :show, status: :ok, location: api_group_url(@group)
      else
        render json: { errors: @group.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @group.destroy
    end

    def invite
      parameters = invite_params

      if @group.add_user parameters[:email]
        render :show, status: :created, location: api_group_url(@group)
      else
        render json: { errors: @group.errors }, status: :unprocessable_entity
      end
    end

    def remove
      @group.remove_user params[:user_id]
    end

    def leave
      current_user.leave_group @group
    end

    private

    def set_and_authorize_group
      @group = model_scope.find(params[:id])
      authorize @group
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name)
    end

    def invite_params
      params.require(:user).permit(:email)
    end

    # Base for all model requests
    def model_scope
      current_user.groups
    end

    def check_creator
      raise ForbiddenAction, t('.not_creator') unless current_user.id == @group.creator_id
    end
  end
end
