class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery

  before_action :authenticate_user!

  private

  def pundit_user
    Context::User.new(current_user, params[:group_id])
  end
end
