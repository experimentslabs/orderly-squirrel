import Vue from 'vue'
import VueResource from 'vue-resource'
import VueI18n from 'vue-i18n'
import App from '../app.vue'
import router from '../routes'
import store from '../store'
import i18nMessages from '../locales'

Vue.use(VueResource)
Vue.use(VueI18n)

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    router,
    store,
    i18n: new VueI18n({ locale: 'en', messages: i18nMessages }),
    render: h => h(App),
  }).$mount()
  document.body.appendChild(app.$el)
})
