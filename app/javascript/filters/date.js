import Moment from 'moment'

export default {
  filters: {
    niceDate: (stringDate) => new Moment(stringDate).format('L'),
  },
}
