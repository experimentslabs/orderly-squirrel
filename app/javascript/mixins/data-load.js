export default {
  data () {
    return {
      loading: false,
      error: false,
      loadActions: [],
    }
  },
  methods: {
    loadData () {
      const lastInit = this.$store.getters.lastInitializationDate

      // Don't load if initialization was made in the 5 last seconds
      if (!lastInit || Date.now() - lastInit < process.env.DATA_LOAD_PAUSE) return

      this.loading = true
      const actions = []

      for (let action of this.loadActions) {
        actions.push(this.$store.dispatch(action))
      }
      Promise.all(actions)
        .catch((error) => { this.error = error })
        .then(() => { this.loading = false })
    },
  },
  created () {
    this.loadData()
  },
}
