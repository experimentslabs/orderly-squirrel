export default {
  data () {
    return {
      displayForm: false,
    }
  },
  methods: {
    edit () {
      this.displayForm = true
    },
    cancelEdition () {
      this.displayForm = false
    },
  },
}
