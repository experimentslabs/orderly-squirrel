export default {
  data () {
    return {
      displayForm: false,
      currentEntity: null,
      mainFormRef: null,
    }
  },
  methods: {
    add () {
      this.currentEntity = null
      this.displayForm = true
    },
    edit (entity) {
      this.currentEntity = entity
      this.displayForm = true
    },
    cancelEdition () {
      this.displayForm = false
      this.currentEntity = null
      if (this.mainFormRef &&
        this.$refs[this.mainFormRef] &&
        this.$refs[this.mainFormRef].reset) {
        this.$refs[this.mainFormRef].reset()
      } else {
        console.warn('IndexEntityEdition mixin used without "mainFormRef" or on a form without "reset()" method.')
      }
    },
  },
}
