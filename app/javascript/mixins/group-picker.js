export default {
  computed: {
    currentGroup () { return this.$store.getters.currentGroup },
  },
  methods: {
    setCurrentGroup (id) {
      if (!this.currentGroup || id !== this.currentGroup.id) {
        this.$store.dispatch('changeCurrentGroup', id)
      }
      this.$emit('navigated')
      this.showList = false
    },
  },
}
