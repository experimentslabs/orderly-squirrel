import Vue from 'vue'
import VueX from 'vuex'
import { createModule, ADD_TOAST_MESSAGE } from 'vuex-toast'
import auth from './modules/auth'
import categories from './modules/categories'
import containers from './modules/containers'
import containersItems from './modules/containers_items'
import groups from './modules/groups'
import items from './modules/items'
import shoppingLists from './modules/shopping_lists'
import shoppingListsItems from './modules/shopping_lists_items'
import users from './modules/users'

Vue.use(VueX)

export default new VueX.Store({
  state: {
    currentGroupId: null,
    // Should be set to true when the whole app needs to be disabled.
    initializing: false,
    lastInitializationDate: null,
    enableLab: false,
  },
  mutations: {
    set_current_group (state, id) {
      state.currentGroupId = id
      localStorage.setItem('currentGroupId', id)
    },
    set_initializing (state, value) {
      if (value === false) state.lastInitializationDate = Date.now()
      state.initializing = value
    },
    set_lab (state, value) {
      state.enableLab = value
      localStorage.setItem('enableLab', value ? '1' : '0')
    },
  },
  actions: {
    initialize: ({ dispatch, commit }) => {
      commit('set_initializing', true)
      return dispatch('loadAuthUser')
        .then((user) => {
          dispatch('initializeLab')
          dispatch(ADD_TOAST_MESSAGE, { text: `Welcome back ${user.name}!`, type: 'primary' })

          return Promise.all([
            dispatch('loadUsers'),
            dispatch('loadGroups'),
          ])
            .then((responses) => {
              const groups = responses[1]
              const localGroupId = localStorage.getItem('currentGroupId')
              let currentGroupId = null

              if (localGroupId) {
                if (groups.find(g => g.id === Number(localGroupId))) currentGroupId = Number(localGroupId)
              } else {
                const index = groups.findIndex(g => g.creator_id === user.id)
                if (index > -1) currentGroupId = groups[index].id
              }

              if (!currentGroupId) return Promise.resolve()

              commit('set_current_group', currentGroupId)

              return dispatch('initializeGroupData')
            })
            .catch((error) => Promise.reject(error))
            .then(() => { commit('set_initializing', false) })
        })
    },
    initializeGroupData: ({ dispatch }) => {
      const calls = [
        dispatch('loadCategories'),
        dispatch('loadContainers'),
        dispatch('loadContainersItems'),
        dispatch('loadItems'),
        dispatch('loadShoppingLists'),
        dispatch('loadShoppingListsItems'),
      ]

      return Promise.all(calls)
    },
    resetStore: ({ dispatch }, groupDataOnly = false) => {
      if (!groupDataOnly) {
        dispatch('resetGroups')
        dispatch('resetUsers')
      }
      dispatch('resetCategories')
      dispatch('resetContainers')
      dispatch('resetContainersItems')
      dispatch('resetItems')
      dispatch('resetShoppingLists')
      dispatch('resetShoppingListsItems')
    },
    changeCurrentGroup ({ dispatch, commit }, id) {
      commit('set_initializing', true)

      return dispatch('resetStore', true)
        .then(() => {
          commit('set_current_group', id)
          return dispatch('initializeGroupData')
            .then(() => { commit('set_initializing', false) })
        })
    },
    setLab: ({ commit }, value) => { commit('set_lab', value) },
    initializeLab: ({ commit }) => {
      commit('set_lab', localStorage.getItem('enableLab') === '1')
    },
  },
  getters: {
    currentGroupId: state => state.currentGroupId,
    appIsInitializing: state => state.initializing,
    lastInitializationDate: state => state.lastInitializationDate,
    enableLab: state => state.enableLab,
  },
  modules: {
    auth,
    categories,
    containers,
    containersItems,
    groups,
    items,
    shoppingLists,
    shoppingListsItems,
    toast: createModule({ dismissInterval: 3000 }),
    users,
  },
})
