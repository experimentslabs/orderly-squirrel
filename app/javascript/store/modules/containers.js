import Vue from 'vue'
import api from '../../lib/api'

export default {
  state: {
    containers: [],
  },
  mutations: {
    set_containers (state, data) {
      state.containers = data
    },
    set_container (state, data) {
      let index = state.containers.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.containers, index, data)
      else state.containers.push(data)
    },
    delete_container (state, id) {
      let index = state.containers.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.containers, index)
    },
    reset_containers (state) {
      state.containers = []
    },
  },
  actions: {
    loadContainers: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/containers`)
        .then(({ body }) => { commit('set_containers', body) })
    },
    saveContainer: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/containers${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { container: payload })
        .then(({ body }) => { commit('set_container', body) })
    },
    destroyContainer: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/containers/${id}`)
        .then(() => { commit('delete_container', id) })
    },
    resetContainers: ({ commit }) => { commit('reset_containers') },
  },
  getters: {
    containers: state => state.containers,
    container: state => (id) => {
      const index = state.containers.findIndex(entry => entry.id === id)
      if (index > -1) return state.containers[index]
      return null
    },
  },
}
