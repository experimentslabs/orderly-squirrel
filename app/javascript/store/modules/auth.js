import api from '../../lib/api'

export default {
  state: {
    authUser: null,
  },
  mutations: {
    set_auth_user (state, data) {
      state.authUser = data
    },
    remove_auth_user (state) {
      state.authUser = null
    },
  },
  actions: {
    loadAuthUser: ({ commit }) => api.get('/api/me')
      .then(({ body }) => {
        commit('set_auth_user', body)
        return body
      })
      .catch((error) => { console.error(error) }),
    logout: ({ commit, dispatch }) => api.delete('/users/sign_out')
      .then(() => {
        commit('remove_auth_user')
        dispatch('resetStore')
          .then(() => { window.location = '/' })
      }),
  },
  getters: {
    authUser: state => state.authUser,
  },
}
