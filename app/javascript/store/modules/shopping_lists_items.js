import Vue from 'vue'
import api from '../../lib/api'

export default {
  state: {
    shoppingListsItems: [],
  },
  mutations: {
    set_shopping_lists_items (state, data) {
      state.shoppingListsItems = data
    },
    set_shopping_lists_item (state, data) {
      let index = state.shoppingListsItems.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.shoppingListsItems, index, data)
      else state.shoppingListsItems.push(data)
    },
    delete_shopping_lists_item (state, id) {
      let index = state.shoppingListsItems.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.shoppingListsItems, index)
    },
    reset_shopping_lists_items (state) {
      state.shoppingListsItems = []
    },
  },
  actions: {
    loadShoppingListsItems: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/shopping_lists_items`)
        .then(({ body }) => { commit('set_shopping_lists_items', body) })
    },
    saveShoppingListsItem: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/shopping_lists_items${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { shopping_lists_item: payload })
        .then(({ body }) => { commit('set_shopping_lists_item', body) })
    },
    destroyShoppingListsItem: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/shopping_lists_items/${id}`)
        .then(() => { commit('delete_shopping_lists_item', id) })
    },
    resetShoppingListsItems: ({ commit }) => { commit('reset_shopping_lists_items') },
  },
  getters: {
    shoppingListsItems: state => state.shoppingListsItems,
    shoppingListsItemsIn: (state, getters) => shoppingListId => state.shoppingListsItems
      .filter(item => item.shopping_list_id === shoppingListId)
      .map(e => { e.item = getters.item(e.item_id); return e }),
    shoppingListsItemsDoneIn: (state, getters) => shoppingListId => getters.shoppingListsItemsIn(shoppingListId).filter(item => item.done),
    shoppingListsItemsOf: state => itemId => state.shoppingListsItems.filter(i => i.item_id === itemId),
    shoppingListsItem: state => (id) => {
      const index = state.shoppingListsItems.findIndex(entry => entry.id === id)
      if (index > -1) return state.shoppingListsItems[index]
      return null
    },
    assignedItems: (state, getters) => {
      const authUser = getters.authUser
      return state.shoppingListsItems.filter(e => e.assignee_id === authUser.id && !e.done)
    },
    // Only returns the ids, not shopping lists
    shoppingListsWithItem: (state, getters) => (itemId) => getters.shoppingListsItemsOf(itemId)
      .map(i => i.shopping_list_id)
      .reduce((previous, current) => {
        if (previous.indexOf(current) === -1) previous.push(current)
        return previous
      }, []),
  },
}
