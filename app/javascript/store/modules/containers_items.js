import Vue from 'vue'
import api from '../../lib/api'

export default {
  state: {
    containersItems: [],
  },
  mutations: {
    set_containers_items (state, data) {
      state.containersItems = data
    },
    set_containers_item (state, data) {
      let index = state.containersItems.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.containersItems, index, data)
      else state.containersItems.push(data)
    },
    delete_containers_item (state, id) {
      let index = state.containersItems.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.containersItems, index)
    },
    reset_containers_items (state) {
      state.containersItems = []
    },
  },
  actions: {
    loadContainersItems: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/containers_items`)
        .then(({ body }) => { commit('set_containers_items', body) })
    },
    saveContainersItem: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/containers_items${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { containers_item: payload })
        .then(({ body }) => { commit('set_containers_item', body) })
    },
    destroyContainersItem: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/containers_items/${id}`)
        .then(() => { commit('delete_containers_item', id) })
    },
    resetContainersItems: ({ commit }) => { commit('reset_containers_items') },
  },
  getters: {
    containersItems: state => state.containersItems,
    containersItemsIn: (state, getters) => containerId => state.containersItems
      .filter(i => i.container_id === containerId)
      .map(e => { e.item = getters.item(e.item_id); return e }),
    containersItemsOf: (state, getters) => itemId => state.containersItems
      .filter(i => i.item_id === itemId)
      .map(i => { i.container = getters.container(i.container_id); return i }),
    containersItem: state => (id) => {
      const index = state.containersItems.findIndex(entry => entry.id === id)
      if (index > -1) return state.containersItems[index]
      return null
    },
    // Only returns the ids, not a container
    containersWithItem: (state, getters) => (itemId) => getters.containersItemsOf(itemId)
      .map(i => i.container_id)
      .reduce((previous, current) => {
        if (previous.indexOf(current) === -1) previous.push(current)
        return previous
      }, []),
    totalStoredAmountOf: (state, getters) => itemId => getters.containersItemsOf(itemId)
      .reduce((previous, current) => previous + current.quantity, 0),
  },
}
