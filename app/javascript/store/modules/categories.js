import Vue from 'vue'
import api from '../../lib/api'
import s from '../../lib/search'

export default {
  state: {
    categories: [],
  },
  mutations: {
    set_categories (state, data) {
      state.categories = data
    },
    set_category (state, data) {
      let index = state.categories.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.categories, index, data)
      else state.categories.push(data)
    },
    delete_category (state, id) {
      let index = state.categories.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.categories, index)
    },
    reset_categories (state) {
      state.categories = []
    },
  },
  actions: {
    loadCategories: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/categories`)
        .then(({ body }) => { commit('set_categories', body) })
    },
    saveCategory: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/categories${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { category: payload })
        .then(({ body }) => { commit('set_category', body) })
    },
    destroyCategory: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/categories/${id}`)
        .then(() => { commit('delete_category', id) })
    },
    resetCategories: ({ commit }) => { commit('reset_categories') },
  },
  getters: {
    categories: state => state.categories,
    category: state => (id) => {
      const index = state.categories.findIndex(entry => entry.id === id)
      if (index > -1) return state.categories[index]
      return null
    },
    filteredCategories: state => (search = '') => state.categories.filter(category => s.levenshteinFilter(search, category.name)),
  },
}
