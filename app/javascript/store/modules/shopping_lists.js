import Vue from 'vue'
import api from '../../lib/api'

export default {
  state: {
    shoppingLists: [],
  },
  mutations: {
    set_shopping_lists (state, data) {
      state.shoppingLists = data
    },
    set_shopping_list (state, data) {
      let index = state.shoppingLists.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.shoppingLists, index, data)
      else state.shoppingLists.push(data)
    },
    delete_shopping_list (state, id) {
      let index = state.shoppingLists.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.shoppingLists, index)
    },
    reset_shopping_lists (state) {
      state.shoppingLists = []
    },
  },
  actions: {
    loadShoppingLists: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/shopping_lists`)
        .then(({ body }) => { commit('set_shopping_lists', body) })
    },
    saveShoppingList: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/shopping_lists${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { shopping_list: payload })
        .then(({ body }) => { commit('set_shopping_list', body) })
    },
    destroyShoppingList: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/shopping_lists/${id}`)
        .then(() => { commit('delete_shopping_list', id) })
    },
    resetShoppingLists: ({ commit }) => { commit('reset_shopping_lists') },
  },
  getters: {
    shoppingLists: state => state.shoppingLists,
    shoppingListsDone: state => state.shoppingLists.filter(shoppingList => shoppingList.done_at),
    shoppingListsTodo: state => state.shoppingLists.filter(shoppingList => !shoppingList.done_at),
    shoppingList: state => (id) => {
      const index = state.shoppingLists.findIndex(entry => entry.id === id)
      if (index > -1) return state.shoppingLists[index]
      return null
    },
  },
}
