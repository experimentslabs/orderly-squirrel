import Vue from 'vue'
import api from '../../lib/api'

export default {
  state: {
    groups: [],
  },
  mutations: {
    set_groups (state, data) {
      state.groups = data
    },
    set_group (state, data) {
      let index = state.groups.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.groups, index, data)
      else state.groups.push(data)
    },
    delete_group (state, id) {
      let index = state.groups.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.groups, index)
    },
  },
  actions: {
    loadGroups: ({ commit }) => api.get('/api/groups')
      .then(({ body }) => {
        commit('set_groups', body)

        return body
      }),
    loadGroup: ({ commit }, id) => api.get(`/api/groups/${id}`)
      .then(({ body }) => {
        commit('set_group', body)

        return body
      }),
    saveGroup: ({ commit }, payload) => {
      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { group: payload })
        .then(({ body }) => {
          commit('set_group', body)
          // Switch to the new group
          commit('set_current_group', body.id)
        })
    },
    destroyGroup: ({ commit, getters }, id) => api.delete(`/api/groups/${id}`)
      .then(() => {
        if (getters.currentGroupId === id) commit('set_current_group', null)
        commit('delete_group', id)
      }),
    leaveGroup: ({ commit, getters }, id) => api.delete(`/api/groups/${id}/leave`)
      .then(() => {
        if (getters.currentGroupId === id) commit('set_current_group', null)
        commit('delete_group', id)
      }),
    removeGroupUser: ({ dispatch, commit }, { groupId, userId }) => api.delete(`/api/groups/${groupId}/remove/${userId}`)
      .then(() => {
        dispatch('loadGroup', groupId)
          .then(() => { commit('delete_user', userId) })
      }),
    inviteGroupUser: ({ dispatch, commit }, { groupId, payload }) => api.post(`/api/groups/${groupId}/invite`, { user: payload })
      .then(({ body }) => {
        dispatch('loadUsers')
          .then(() => {
            commit('set_group', body)
          })
      }),
  },
  getters: {
    groups: state => state.groups,
    currentGroup: (state, getters) => getters.group(getters.currentGroupId),
    groupsCount: state => state.groups.length,
    group: state => (id) => {
      const index = state.groups.findIndex(entry => entry.id === id)
      if (index > -1) return state.groups[index]
      return null
    },
  },
}
