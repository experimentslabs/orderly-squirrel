import Vue from 'vue'
import api from '../../lib/api'
import { ADD_TOAST_MESSAGE } from 'vuex-toast'

export default {
  state: {
    users: [],
  },
  mutations: {
    set_users (state, data) {
      state.users = data
    },
    delete_user (state, id) {
      let index = state.users.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.users, index)
    },
    reset_users (state) {
      state.users = []
    },
  },
  actions: {
    loadUsers: ({ commit }) => {
      return api.get(`/api/users`)
        .then(({ body }) => { commit('set_users', body) })
    },
    saveUser: ({ dispatch, commit }, payload) => {
      let finalPayload = { user: payload }
      let options = {}
      if (payload['avatar']) {
        finalPayload = new FormData()
        for (let key of Object.keys(payload)) {
          finalPayload.append(`user[${key}]`, payload[key])
        }
        options = { headers: { 'content-type': 'multipart/form-data' } }
      }
      return api.patch(`/api/me`, finalPayload, options)
        .then(({ body }) => {
          commit('set_auth_user', body)
          dispatch(ADD_TOAST_MESSAGE, { text: 'Profile updated' })
        })
    },
    destroyUser: ({ commit }, id) => api.delete(`/api/me`)
      .then(() => { commit('delete_user', id) }),
    resetUsers: ({ commit }) => { commit('reset_users') },
  },
  getters: {
    users: state => state.users,
    user: state => (id) => {
      const index = state.users.findIndex(entry => entry.id === id)
      if (index > -1) return state.users[index]
      return null
    },
    usersInCurrentGroup: (state, getters) => getters.usersByIds(getters.currentGroup.user_ids)
      .sort((a, b) => a.name.localeCompare(b.name)),
    usersByIds: state => ids => state.users.filter(user => ids.indexOf(user.id) > -1),
  },
}
