import Vue from 'vue'
import api from '../../lib/api'
import s from '../../lib/search'

export default {
  state: {
    items: [],
  },
  mutations: {
    set_items (state, data) {
      state.items = data
    },
    set_item (state, data) {
      let index = state.items.findIndex(e => e.id === data.id)
      if (index > -1) Vue.set(state.items, index, data)
      else state.items.push(data)
    },
    delete_item (state, id) {
      let index = state.items.findIndex(e => e.id === id)
      if (index > -1) Vue.delete(state.items, index)
    },
    reset_items (state) {
      state.items = []
    },
  },
  actions: {
    loadItems: ({ getters, commit }) => {
      const groupId = getters.currentGroupId

      return api.get(`/api/groups/${groupId}/items`)
        .then(({ body }) => { commit('set_items', body) })
    },
    saveItem: ({ getters, commit }, payload) => {
      const groupId = getters.currentGroupId

      const action = payload.id ? 'patch' : 'post'
      const url = `/api/groups/${groupId}/items${payload.id ? `/${payload.id}` : ''}`
      return api[action](url, { item: payload })
        .then(({ body }) => { commit('set_item', body) })
    },
    destroyItem: ({ getters, commit }, id) => {
      const groupId = getters.currentGroupId

      return api.delete(`/api/groups/${groupId}/items/${id}`)
        .then(() => { commit('delete_item', id) })
    },
    resetItems: ({ commit }) => { commit('reset_items') },
  },
  getters: {
    items: state => state.items,
    item: state => (id) => {
      const index = state.items.findIndex(entry => entry.id === id)
      if (index > -1) return state.items[index]
      return null
    },
    filteredItems: state => (search = '') => state.items.filter(item => s.levenshteinFilter(search, item.name)),
    itemsInCategory: state => categoryId => state.items.filter(item => item.category_id === categoryId),
  },
}
