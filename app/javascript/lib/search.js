import levenshtein from 'js-levenshtein'

export default {
  levenshteinFilter (search, compared, minScoreStart = 2, minScoreComplete = 5) {
    if (!search || search.length === 0) return true
    // Simple matcher
    const match = new RegExp(search, 'i').test(compared)
    // Levenshtein on the beginning
    const startScore = levenshtein(compared.substring(0, search.length), search) < minScoreStart
    // Levenshtein on whole string
    const completeScore = levenshtein(compared, search) < minScoreComplete
    return match || startScore || completeScore
  },
}
