export default {
  hasOwnNestedProperty (object, propertyPath) {
    if (!propertyPath) return false

    const properties = propertyPath.split('.')

    for (let i = 0; i < properties.length; i++) {
      let prop = properties[i]

      if (!object || !object.hasOwnProperty(prop)) {
        return false
      } else {
        object = object[prop]
      }
    }

    return true
  },
}
