import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

function apiCall (method, url, payload = {}, options = {}) {
  return Vue.http[method](`${url}`, payload, options)
}

export default {
  get: (url, payload, options) => apiCall('get', url, payload, options),
  post: (url, payload, options) => apiCall('post', url, payload, options),
  put: (url, payload, options) => apiCall('put', url, payload, options),
  patch: (url, payload, options) => apiCall('patch', url, payload, options),
  delete: (url, payload, options) => apiCall('delete', url, payload, options),
}
