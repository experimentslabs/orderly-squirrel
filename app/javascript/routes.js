import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "containers" */ './templates/containers/Index'),
    },
    {
      path: '/items',
      name: 'items',
      component: () => import(/* webpackChunkName: "items" */ './templates/items/Index'),
    },
    {
      path: '/me',
      name: 'me',
      component: () => import(/* webpackChunkName: "me" */ './templates/users/Me.vue'),
    },
    {
      path: '/groups',
      name: 'groups',
      component: () => import(/* webpackChunkName: "groups" */ './templates/groups/Index'),
    },
    {
      path: '/categories',
      name: 'categories',
      component: () => import(/* webpackChunkName: "categories" */ './templates/categories/Index'),
    },
    {
      path: '/shopping_lists',
      name: 'shopping_lists',
      component: () => import(/* webpackChunkName: "shopping_lists" */ './templates/shopping_lists/Index'),
    },
    {
      path: '/shopping_lists/:id',
      name: 'shopping_list',
      component: () => import(/* webpackChunkName: "shopping_list" */ './templates/shopping_lists/Show'),
    },
  ],
})

function enableLab () {
  return localStorage.getItem('enableLab') === '1'
}

router.beforeEach((to, from, next) => {
  if (to.meta.experimental && !enableLab()) {
    next({ name: 'home' })
  } else {
    next()
  }
})

export default router
