json.extract! user, :id, :name, :email, :created_at, :updated_at, :unconfirmed_email

if user.avatar.attached?
  json.avatar_150 url_for(user.avatar_representation(150))
  json.avatar_50 url_for(user.avatar_representation(50))
else
  json.avatar_150 nil
  json.avatar_50 nil
end
