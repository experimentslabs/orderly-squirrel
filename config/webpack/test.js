process.env.NODE_ENV = process.env.NODE_ENV || 'development'
process.env.DATA_LOAD_PAUSE = process.env.DATA_LOAD_PAUSE || '0'

const environment = require('./environment')

module.exports = environment.toWebpackConfig()
