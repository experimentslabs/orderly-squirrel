process.env.NODE_ENV = process.env.NODE_ENV || 'production'
process.env.DATA_LOAD_PAUSE = process.env.DATA_LOAD_PAUSE || '5000'

const environment = require('./environment')

module.exports = environment.toWebpackConfig()
