Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions:      'auth/sessions',
    registrations: 'auth/registrations',
  }

  root to: 'home#index'

  get '/docs/api', to: 'docs#api'

  namespace :api do
    # Groups
    post '/groups/:id/invite', to: 'groups#invite'
    delete '/groups/:id/remove/:user_id', to: 'groups#remove'
    delete '/groups/:id/leave', to: 'groups#leave'
    resources :groups, except: [:new, :edit] do
      resources :categories, except: [:new, :edit]
      resources :containers, except: [:new, :edit]
      resources :containers_items, except: [:new, :edit]
      resources :items, except: [:new, :edit]
      resources :logs, only: [:index, :show]
      resources :shopping_lists_items, except: [:new, :edit]
      resources :shopping_lists, except: [:new, :edit]
    end
    # Users
    get '/me', to: 'users#me'
    patch '/me', to: 'users#update'
    put '/me', to: 'users#update'
    resources :users, except: [:destroy, :create, :update]
  end
end
