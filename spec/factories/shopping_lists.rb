FactoryBot.define do
  factory :shopping_list do
    name { Faker::Date.between(from: 2.days.ago, to: Time.zone.today) }
    group
  end
end
