FactoryBot.define do
  factory :containers_item do
    after(:build) do |entity|
      unless entity.container && entity.item

        group = if entity.container
                  entity.container.group
                elsif entity.item
                  entity.item.group
                else
                  FactoryBot.create :group
                end

        unless entity.container
          container        = create :container, group: group
          entity.container = container
        end

        unless entity.item
          item        = create :item, group: group
          entity.item = item
        end
      end
    end
  end
end
