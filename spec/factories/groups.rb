FactoryBot.define do
  # Note: Creating a group with this factory will create a creator, which
  # will create its first group too.
  factory :group do
    name { "#{Faker::Name.last_name} family" }
    association :creator, factory: :user
  end
end
