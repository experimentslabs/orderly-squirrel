FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    name { Faker::Name.first_name }
    password { 'password' }

    factory :user_active do
      confirmed_at { Time.zone.now }

      factory :user_known do
        email { 'user@example.com' }
      end
    end
  end
end
