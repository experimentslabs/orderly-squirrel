FactoryBot.define do
  factory :shopping_lists_item do
    after(:build) do |entity|
      unless entity.shopping_list && entity.item

        group = if entity.shopping_list
                  entity.shopping_list.group
                elsif entity.item
                  entity.item.group
                else
                  FactoryBot.create :group
                end

        unless entity.shopping_list
          shopping_list        = create :shopping_list, group: group
          entity.shopping_list = shopping_list
        end

        unless entity.item
          item        = create :item, group: group
          entity.item = item
        end
      end
    end
  end
end
