FactoryBot.define do
  factory :container do
    name { Faker::House.furniture }
    group
  end
end
