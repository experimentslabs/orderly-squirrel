FactoryBot.define do
  factory :item do
    name { Faker::Food.vegetables }
    group
  end
end
