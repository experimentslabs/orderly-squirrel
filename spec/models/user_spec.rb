require 'rails_helper'

RSpec.describe User, type: :model do
  let(:creator) { FactoryBot.create :user_active }

  it 'creates a group when created' do
    expect do
      FactoryBot.create :user_active
    end.to change(Group, :count).by(1)
  end

  before do
    FactoryBot.create_list :user_active, 2
  end

  context 'two users in same group' do
    let(:user_in_group) { FactoryBot.create :user_active, groups: [creator.groups.first] }

    before do
      user_in_group # create users
    end

    it 'displays two known users' do
      expect(creator.known_users.count).to eq 2
      expect(User.known_users(creator.id).count).to eq 2
    end
    it 'knows the other user' do
      expect(creator.know?(user_in_group.id)).to be_truthy
    end
  end
  context 'two users in different groups' do
    let(:other_user) { FactoryBot.create :user_active }

    before do
      other_user # create users
    end

    it 'displays one know user, which is the current one' do
      known_users = creator.known_users
      expect(known_users.count).to eq 1
      expect(known_users.first.id).to eq creator.id
      expect(User.known_users(creator.id).count).to eq 1
    end
    it "don't know the other user" do
      expect(creator.know?(other_user.id)).to be_falsey
    end
  end
end
