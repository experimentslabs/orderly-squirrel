require 'rails_helper'

RSpec.describe Container, type: :model do
  describe 'check the user performing action and the group' do
    let(:user) { FactoryBot.create :user }
    let(:container) { FactoryBot.build(:container) }

    it "don't create the container if user don't belongs to the group" do
      container.group = FactoryBot.create :group
      container.user  = user

      expect do
        container.save
      end.to change(Container, :count).by(0)
    end

    it 'creates the container if user belongs to the group' do
      container.group = user.groups.first
      container.user  = user
      expect do
        container.save
      end.to change(Container, :count).by(1)
    end
  end
end
