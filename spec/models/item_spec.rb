require 'rails_helper'

RSpec.describe Item, type: :model do
  describe 'check the group of associated category' do
    it "can't have a category from a group of another user" do
      main_user      = FactoryBot.create :user_active
      main_group     = main_user.groups.first
      other_user     = FactoryBot.create :user_active
      other_group    = other_user.groups.first
      other_category = FactoryBot.create :category, group: other_group, user: other_user

      expect do
        FactoryBot.create :item, group: main_group, user: main_user, category: other_category
      end.to raise_error 'Validation failed: Item is not in the same group as category'
    end

    it "can't have a category from another group of the same user" do
      main_user  = FactoryBot.create :user_active
      main_group = main_user.groups.first
      main_user.groups.push FactoryBot.create :group
      second_group = main_user.groups.last
      category     = FactoryBot.create :category, group: second_group, user: main_user

      expect do
        FactoryBot.create :item, group: main_group, user: main_user, category: category
      end.to raise_error 'Validation failed: Item is not in the same group as category'
    end
  end

  describe 'check the user performing action and the group' do
    let(:user) { FactoryBot.create :user }
    let(:item) { FactoryBot.build(:item) }

    it "don't create the item if user don't belongs to the group" do
      item.group = FactoryBot.create :group
      item.user  = user
      expect do
        item.save
      end.to change(Item, :count).by(0)
    end

    it 'creates the item if user belongs to the group' do
      item.group = user.groups.first
      item.user  = user
      expect do
        item.save
      end.to change(Item, :count).by(1)
    end
  end
end
