require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'when user is in the group' do
    let(:creator) { FactoryBot.create :user_active }
    let(:other_user) { FactoryBot.create :user_active }
    let(:group) { creator.groups.first }

    before do
      # Initialize creator, group and other user
      group
      other_user
    end

    it 'adds user by specifying an email' do
      expect do
        group.add_user other_user.email
      end.to change(GroupsUser, :count).by(1)
    end

    it "can't add the same user twice" do
      group.add_user other_user.email
      expect do
        group.add_user other_user.email
      end.to raise_error ActiveRecord::RecordInvalid
    end

    it 'removes user by specifying an ID' do
      group.users.push other_user

      expect do
        group.remove_user other_user.id
      end.to change(GroupsUser, :count).by(-1)
    end

    context 'when user is a creator of the group' do
      it "can't leave the group" do
        expect do
          group.remove_user group.creator_id
        end.to raise_error ForbiddenAction
      end

      it "can't remove itself from users" do
        expect do
          group.remove_user group.creator_id
        end.to raise_error ForbiddenAction
      end
    end

    context 'when user is not a creator of the group' do
      let(:user_in_group) { FactoryBot.create :user_active, groups: [creator.groups.first] }

      it 'can leave the group' do
        expect do
          group.remove_user user_in_group.id
        end.to change(GroupsUser, :count).by(1)
      end
    end
  end
end
