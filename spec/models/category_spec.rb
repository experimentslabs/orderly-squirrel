require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'check the user performing action and the group' do
    let(:user) { FactoryBot.create :user }
    let(:category) { FactoryBot.build(:category) }

    it "don't create the category if user don't belongs to the group" do
      category.group = FactoryBot.create :group
      category.user  = user
      expect do
        category.save
      end.to change(Category, :count).by(0)
    end

    it 'creates the category if user belongs to the group' do
      category.group = user.groups.first
      category.user  = user
      expect do
        category.save
      end.to change(Category, :count).by(1)
    end
  end
end
