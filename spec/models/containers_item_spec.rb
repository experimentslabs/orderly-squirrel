require 'rails_helper'

RSpec.describe ContainersItem, type: :model do
  it "can't be created with elements of different groups" do
    user         = FactoryBot.create(:user_active)
    first_group  = user.groups.first
    second_group = FactoryBot.create :group, creator: user
    container    = FactoryBot.create :container, user: user, group: first_group
    item         = FactoryBot.create :item, user: user, group: second_group

    expect do
      ContainersItem.create! container: container, item: item
    end.to raise_error(/container and item are not in the same group/)
  end

  it "can't have the same item more than once in the same container" do
    user      = FactoryBot.create(:user_active)
    group     = user.groups.first
    container = FactoryBot.create :container, user: user, group: group
    item      = FactoryBot.create :item, user: user, group: group
    ContainersItem.create! container: container, item: item

    expect do
      ContainersItem.create! container: container, item: item
    end.to raise_error(/Item has already been taken/)
  end
end
