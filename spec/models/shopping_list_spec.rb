require 'rails_helper'

RSpec.describe ShoppingList, type: :model do
  describe 'check the user performing action and the group' do
    let(:user) { FactoryBot.create :user }
    let(:shopping_list) { FactoryBot.build(:shopping_list) }

    it "don't create the shopping list if user don't belongs to the group" do
      shopping_list.group = FactoryBot.create :group
      shopping_list.user  = user
      expect do
        shopping_list.save
      end.to change(ShoppingList, :count).by(0)
    end

    it 'creates the shopping list if user belongs to the group' do
      shopping_list.group = user.groups.first
      shopping_list.user  = user
      expect do
        shopping_list.save
      end.to change(ShoppingList, :count).by(1)
    end
  end
end
