require 'rails_helper'

RSpec.describe ShoppingListsItem, type: :model do
  include ActiveJob::TestHelper

  let(:user) { FactoryBot.create(:user_active) }
  let(:group) { user.groups.first }
  let(:shopping_list) { FactoryBot.create :shopping_list, user: user, group: group }
  let(:item) { FactoryBot.create :item, user: user, group: group }

  context 'creating a new association' do
    it "can't be created with elements of different groups" do
      second_group = FactoryBot.create :group, creator: user
      item         = FactoryBot.create :item, user: user, group: second_group

      expect do
        ShoppingListsItem.create! shopping_list: shopping_list, item: item
      end.to raise_error(/shopping_list and item are not in the same group/)
    end

    it "can't have the same item more than once in the same shopping list" do
      ShoppingListsItem.create! shopping_list: shopping_list, item: item

      expect do
        ShoppingListsItem.create! shopping_list: shopping_list, item: item
      end.to raise_error(/Item has already been taken/)
    end

    context 'when quantity is not specified' do
      it 'sets the default quantity if not specified' do
        shopping_list_item = ShoppingListsItem.create! shopping_list: shopping_list, item: item

        expect(shopping_list_item.quantity).to eq 1
      end

      it 'sets the quantity depending on the alert threshold and stashed amount' do
        item.update alert_threshold: 2
        container = FactoryBot.create :container, user: user, group: group
        FactoryBot.create :containers_item, item: item, container: container, quantity: 0

        shopping_list_item = ShoppingListsItem.create! shopping_list: shopping_list, item: item
        expect(shopping_list_item.quantity).to eq 2
      end
    end

    context 'when quantity is specified' do
      it 'uses the given quantity' do
        shopping_list_item = ShoppingListsItem.create! shopping_list: shopping_list, item: item, quantity: 20

        expect(shopping_list_item.quantity).to eq 20
      end
    end

    context 'when assigning someone' do
      it 'sends an email to the assigned person' do
        item # Create item, user and group
        expect do
          perform_enqueued_jobs do
            FactoryBot.create :shopping_lists_item, item: item, shopping_list: shopping_list, assignee: user
          end
        end.to change(ActionMailer::Base.deliveries, :count).by(1)
      end
    end

    context 'when removing assignment' do
      it 'does not sent an email' do
        item # Create item, user and group
        shopping_list_item = FactoryBot.create :shopping_lists_item, item: item, shopping_list: shopping_list, assignee: user
        expect do
          perform_enqueued_jobs do
            shopping_list_item.update assignee_id: nil
          end
        end.to change(ActionMailer::Base.deliveries, :count).by(0)
      end
    end

    context 'when changing the assignee' do
      it 'sends an email to the assigned person' do
        item # Create item, user and group
        other_user         = FactoryBot.create :user_active, groups: [group]
        shopping_list_item = FactoryBot.create :shopping_lists_item, item: item, shopping_list: shopping_list, assignee: user

        expect do
          perform_enqueued_jobs do
            shopping_list_item.update assignee: other_user
          end
        end.to change(ActionMailer::Base.deliveries, :count).by(1)
      end
    end
  end
end
