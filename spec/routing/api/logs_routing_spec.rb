require 'rails_helper'

RSpec.describe Api::LogsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/groups/1/logs').to route_to('api/logs#index', group_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/groups/1/logs/1').to route_to('api/logs#show', group_id: '1', id: '1')
    end

    it 'does not routes to #new' do
      expect(get: '/api/groups/1/logs/new').not_to route_to('api/logs#new', group_id: '1')
    end

    it 'does not routes to #create' do
      expect(post: '/api/groups/1/logs').not_to route_to('api/logs#create', group_id: '1')
    end

    it 'does not routes to #edit' do
      expect(get: '/api/groups/1/logs/1/edit').not_to route_to('api/logs#edit', group_id: '1', id: '1')
    end

    it 'does not routes to #update via PATCH' do
      expect(patch: '/api/groups/1/logs').not_to route_to('api/logs#update', group_id: '1', id: '1')
    end

    it 'does not routes to #update via PUT' do
      expect(put: '/api/groups/1/logs').not_to route_to('api/logs#update', group_id: '1', id: '1')
    end

    it 'does not routes to #destroy' do
      expect(delete: '/api/groups/1/logs').not_to route_to('api/logs#destroy', group_id: '1', id: '1')
    end
  end
end
