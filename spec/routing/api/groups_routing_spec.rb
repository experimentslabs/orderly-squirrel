require 'rails_helper'

RSpec.describe Api::GroupsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/groups').to route_to('api/groups#index')
    end

    it 'routes to #show' do
      expect(get: '/api/groups/1').to route_to('api/groups#show', id: '1')
    end

    it 'does not routes to #new' do
      expect(get: '/api/groups/new').not_to route_to('api/groups#new')
    end

    it 'routes to #create' do
      expect(post: '/api/groups').to route_to('api/groups#create')
    end

    it 'does not routes to #edit' do
      expect(get: '/api/groups/1/edit').not_to route_to('api/groups#edit', id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/groups/1').to route_to('api/groups#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/groups/1').to route_to('api/groups#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/groups/1').to route_to('api/groups#destroy', id: '1')
    end

    it 'routes to #invite' do
      expect(post: '/api/groups/1/invite').to route_to('api/groups#invite', id: '1')
    end

    it 'routes to #remove' do
      expect(delete: '/api/groups/1/remove/1').to route_to('api/groups#remove', id: '1', user_id: '1')
    end
  end
end
