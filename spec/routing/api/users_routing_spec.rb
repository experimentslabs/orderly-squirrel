require 'rails_helper'

RSpec.describe Api::UsersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/users').to route_to('api/users#index')
    end

    it 'routes to #show' do
      expect(get: '/api/users/1').to route_to('api/users#show', id: '1')
    end

    it 'routes to #show current user' do
      expect(get: '/api/me').to route_to('api/users#me')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/me').to route_to('api/users#update')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/me').to route_to('api/users#update')
    end

    it 'does not routes to #destroy' do
      expect(delete: '/api/users/1').not_to be_routable
    end
  end
end
