require 'rails_helper'

RSpec.describe Api::ContainersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/groups/1/containers').to route_to('api/containers#index', group_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/groups/1/containers/1').to route_to('api/containers#show', group_id: '1', id: '1')
    end

    it 'does not routes to #new' do
      expect(get: '/api/groups/1/containers/new').not_to route_to('api/containers#new', group_id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/groups/1/containers').to route_to('api/containers#create', group_id: '1')
    end

    it 'does not routes to #edit' do
      expect(get: '/api/groups/1/containers/1/edit').not_to route_to('api/containers#edit', group_id: '1', id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/groups/1/containers/1').to route_to('api/containers#update', group_id: '1', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/groups/1/containers/1').to route_to('api/containers#update', group_id: '1', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/groups/1/containers/1').to route_to('api/containers#destroy', group_id: '1', id: '1')
    end
  end
end
