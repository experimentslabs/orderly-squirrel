require 'rails_helper'

RSpec.describe Api::ShoppingListsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/groups/1/shopping_lists').to route_to('api/shopping_lists#index', group_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/groups/1/shopping_lists/1').to route_to('api/shopping_lists#show', group_id: '1', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/groups/1/shopping_lists').to route_to('api/shopping_lists#create', group_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/groups/1/shopping_lists/1').to route_to('api/shopping_lists#update', group_id: '1', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/groups/1/shopping_lists/1').to route_to('api/shopping_lists#update', group_id: '1', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/groups/1/shopping_lists/1').to route_to('api/shopping_lists#destroy', group_id: '1', id: '1')
    end
  end
end
