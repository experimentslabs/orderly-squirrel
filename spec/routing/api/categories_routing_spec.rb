require 'rails_helper'

RSpec.describe Api::CategoriesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/groups/1/categories').to route_to('api/categories#index', group_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/groups/1/categories/1').to route_to('api/categories#show', group_id: '1', id: '1')
    end

    it 'does not routes to #new' do
      expect(get: '/api/groups/1/categories/new').not_to route_to('api/categories#new', group_id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/groups/1/categories').to route_to('api/categories#create', group_id: '1')
    end

    it 'does not routes to #edit' do
      expect(get: '/api/groups/1/categories/1/edit').not_to route_to('api/categories#edit', group_id: '1', id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/groups/1/categories/1').to route_to('api/categories#update', group_id: '1', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/groups/1/categories/1').to route_to('api/categories#update', group_id: '1', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/groups/1/categories/1').to route_to('api/categories#destroy', group_id: '1', id: '1')
    end
  end
end
