require 'rails_helper'

RSpec.describe Api::ShoppingListsItemsController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    item          = FactoryBot.create :item, user: current_user, group: group
    shopping_list = FactoryBot.create :shopping_list, user: current_user, group: group
    FactoryBot.build(:shopping_lists_item, shopping_list: shopping_list, item: item).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { shopping_list_id: 0 }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with all elements controllable by the user' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new ShoppingListsItem' do
          expect do
            post :create, params: { group_id: group.to_param, shopping_lists_item: valid_attributes }, session: valid_session, format: :json
          end.to change(ShoppingListsItem, :count).by 1
        end
      end

      context 'with invalid params' do
        it 'does not create the shopping list item' do
          expect do
            post :create, params: { group_id: group.to_param, shopping_lists_item: invalid_attributes }, session: valid_session, format: :json
          end.to change(ShoppingListsItem, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { quantity: 10 }
        end

        it 'updates the requested shopping list item' do
          shopping_lists_item = ShoppingListsItem.create! valid_attributes
          put :update, params: { group_id: group.to_param, id: shopping_lists_item.to_param, shopping_lists_item: new_attributes }, session: valid_session, format: :json
          shopping_lists_item.reload
          expect(shopping_lists_item.quantity).to eq 10
        end
      end

      context 'with invalid params' do
        it 'does not update the shopping list item' do
          shopping_lists_item     = ShoppingListsItem.create! valid_attributes
          old_shopping_lists_item = shopping_lists_item.clone

          put :update, params: { group_id: group.to_param, id: shopping_lists_item.to_param, shopping_lists_item: invalid_attributes }, session: valid_session, format: :json
          shopping_lists_item.reload

          expect(shopping_lists_item).to eq old_shopping_lists_item
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested shopping item' do
        shopping_lists_item = ShoppingListsItem.create! valid_attributes
        expect do
          delete :destroy, params: { group_id: group.to_param, id: shopping_lists_item.to_param }, session: valid_session, format: :json
        end.to change(ShoppingListsItem, :count).by(-1)
      end
    end
  end
end
