require 'rails_helper'

RSpec.describe Api::ShoppingListsController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    FactoryBot.build(:shopping_list).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with a group in which the user is' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new ShoppingList' do
          expect do
            post :create, params: { group_id: group.id, shopping_list: valid_attributes }, session: valid_session, format: :json
          end.to change(ShoppingList, :count).by 1
        end
      end

      context 'with invalid params' do
        it 'does not create a new shopping_list' do
          expect do
            post :create, params: { group_id: group.id, shopping_list: invalid_attributes }, session: valid_session, format: :json
          end.to change(ShoppingList, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { name: 'Furniture for the attic' }
        end

        it 'updates the requested shopping_list' do
          shopping_list = FactoryBot.create :shopping_list, user: current_user, group: group
          put :update, params: { group_id: group.id, id: shopping_list.to_param, shopping_list: new_attributes }, session: valid_session, format: :json
          shopping_list.reload
          expect(shopping_list.name).to eq 'Furniture for the attic'
        end
      end

      context 'with invalid params' do
        it 'does not update the shopping_list' do
          shopping_list     = FactoryBot.create :shopping_list, user: current_user, group: group
          old_shopping_list = shopping_list.clone

          put :update, params: { group_id: group.id, id: shopping_list.to_param, shopping_list: invalid_attributes }, session: valid_session, format: :json
          shopping_list.reload

          expect(shopping_list).to eq old_shopping_list
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested shopping_list' do
        shopping_list = FactoryBot.create :shopping_list, user: current_user, group: group
        expect do
          delete :destroy, params: { group_id: group.id, id: shopping_list.to_param }, session: valid_session, format: :json
        end.to change(ShoppingList, :count).by(-1)
      end
    end
  end
end
