require 'rails_helper'

RSpec.describe Api::CategoriesController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    FactoryBot.build(:category).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with a group in which the user is' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new Category' do
          expect do
            post :create, params: { group_id: group.to_param, category: valid_attributes }, session: valid_session, format: :json
          end.to change(Category, :count).by 1
        end
      end

      context 'with invalid params' do
        it 'does not create a new category' do
          expect do
            post :create, params: { group_id: group.to_param, category: invalid_attributes }, session: valid_session, format: :json
          end.to change(Category, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { name: 'Vegetables' }
        end

        it 'updates the requested category' do
          category = FactoryBot.create :category, group: group, user: current_user

          put :update, params: { group_id: group.to_param, id: category.to_param, category: new_attributes }, session: valid_session, format: :json
          category.reload

          expect(category.name).to eq 'Vegetables'
        end
      end

      context 'with invalid params' do
        it 'does not update the category' do
          category     = FactoryBot.create :category, group: group, user: current_user
          old_category = category.clone

          put :update, params: { group_id: group.to_param, id: category.to_param, category: invalid_attributes }, session: valid_session, format: :json
          category.reload

          expect(category).to eq old_category
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested category' do
        category = FactoryBot.create :category, group: group, user: current_user
        expect do
          delete :destroy, params: { group_id: group.to_param, id: category.to_param }, session: valid_session, format: :json
        end.to change(Category, :count).by(-1)
      end
    end
  end
end
