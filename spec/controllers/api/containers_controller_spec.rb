require 'rails_helper'

RSpec.describe Api::ContainersController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    FactoryBot.build(:container).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with a group in which the user is' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new Container' do
          expect do
            post :create, params: { group_id: group.to_param, container: valid_attributes }, session: valid_session, format: :json
          end.to change(Container, :count).by(1)
        end
      end

      context 'with invalid params' do
        it 'creates does not create a new Container' do
          expect do
            post :create, params: { group_id: group.to_param, container: invalid_attributes }, session: valid_session, format: :json
          end.to change(Container, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { name: 'Vegetables' }
        end

        it 'updates the requested container' do
          container = FactoryBot.create :container, group: group, user: current_user

          put :update, params: { group_id: group.to_param, id: container.to_param, container: new_attributes }, session: valid_session, format: :json
          container.reload
          expect(container.name).to eq 'Vegetables'
        end
      end

      context 'with invalid params' do
        it 'does not update the category' do
          container     = FactoryBot.create :container, group: group, user: current_user
          old_container = container.clone

          put :update, params: { group_id: group.to_param, id: container.to_param, container: invalid_attributes }, session: valid_session, format: :json
          container.reload

          expect(container).to eq old_container
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested container' do
        container = FactoryBot.create :container, group: group, user: current_user
        expect do
          delete :destroy, params: { group_id: group.to_param, id: container.to_param }, session: valid_session, format: :json
        end.to change(Container, :count).by(-1)
      end
    end
  end
end
