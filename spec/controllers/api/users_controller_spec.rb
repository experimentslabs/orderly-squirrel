require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }

  let(:valid_attributes) do
    FactoryBot.build(:user).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  describe 'PUT #update' do
    let(:new_attributes) do
      { name: 'A lovely name' }
    end

    context 'with valid params' do
      it 'updates the current user' do
        put :update, params: { user: new_attributes }, session: valid_session, format: :json
        current_user.reload
        expect(current_user.name).to eq('A lovely name')
      end
    end

    context 'with invalid params' do
      it 'does not update the current user' do
        old_current_user = current_user.clone

        put :update, params: { user: invalid_attributes }, session: valid_session, format: :json
        current_user.reload

        expect(current_user).to eq old_current_user
      end
    end
  end
end
