require 'rails_helper'

RSpec.describe Api::ContainersItemsController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    item      = FactoryBot.create :item, user: current_user, group: group
    container = FactoryBot.create :container, user: current_user, group: group
    FactoryBot.build(:containers_item, container: container, item: item).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { container_id: 0 }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with all elements controllable by the user' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new ContainersItem' do
          expect do
            post :create, params: { group_id: group.to_param, containers_item: valid_attributes }, session: valid_session, format: :json
          end.to change(ContainersItem, :count).by 1
        end
      end

      context 'with invalid params' do
        it 'does not create a new containers_item' do
          expect do
            post :create, params: { group_id: group.to_param, containers_item: invalid_attributes }, session: valid_session, format: :json
          end.to change(ContainersItem, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { quantity: 10 }
        end

        it 'updates the requested containers_item' do
          containers_item = ContainersItem.create! valid_attributes
          put :update, params: { group_id: group.to_param, id: containers_item.to_param, containers_item: new_attributes }, session: valid_session, format: :json
          containers_item.reload
          expect(containers_item.quantity).to eq 10
        end
      end

      context 'with invalid params' do
        it 'does not update the containers_item' do
          containers_item     = ContainersItem.create! valid_attributes
          old_containers_item = containers_item.clone

          put :update, params: { group_id: group.to_param, id: containers_item.to_param, containers_item: invalid_attributes }, session: valid_session, format: :json
          old_containers_item.reload

          expect(containers_item).to eq old_containers_item
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested containers_item' do
        containers_item = ContainersItem.create! valid_attributes
        expect do
          delete :destroy, params: { group_id: group.to_param, id: containers_item.to_param }, session: valid_session, format: :json
        end.to change(ContainersItem, :count).by(-1)
      end
    end
  end
end
