require 'rails_helper'

RSpec.describe Api::GroupsController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:other_user) { FactoryBot.create :user_active }

  let(:valid_attributes) do
    FactoryBot.build(:group).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:new_attributes) do
    { name: 'Some pretty name' }
  end
  let(:valid_session) { {} }

  before do
    other_user # Create user
    sign_in current_user
  end

  # Anyone can create a group
  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new group with user in it' do
        expect do
          post :create, params: { group: valid_attributes }, session: valid_session, format: :json
        end.to change(Group, :count).by 1
        expect(Group.last.creator_id).to eq current_user.id
        expect(Group.last.user_ids).to eq [current_user.id]
      end
    end

    context 'with invalid params' do
      it 'does not create a new group' do
        expect do
          post :create, params: { group: invalid_attributes }, session: valid_session, format: :json
        end.to change(Group, :count).by 0
      end
    end
  end

  context 'with current user being in the group' do
    context 'user is the creator of the group' do
      let(:group) { current_user.groups.first }

      describe 'PUT #update' do
        context 'with valid params' do
          it 'updates the requested group' do
            put :update, params: { id: group.to_param, group: new_attributes }, session: valid_session, format: :json
            group.reload
            expect(group.name).to eq 'Some pretty name'
          end

          it 'renders a JSON response with the group' do
            put :update, params: { id: group.to_param, group: valid_attributes }, session: valid_session, format: :json
            expect(response).to have_http_status(:ok)
            expect(response.content_type).to eq('application/json')
          end
        end

        context 'with invalid params' do
          it 'does not update the group' do
            old_group = group.clone

            put :update, params: { id: group.to_param, group: invalid_attributes }, session: valid_session, format: :json
            group.reload

            expect(group).to eq old_group
          end
        end
      end

      describe 'DELETE #destroy' do
        it 'destroys the requested group' do
          expect do
            delete :destroy, params: { id: group.to_param }, session: valid_session, format: :json
          end.to change(Group, :count).by(-1)
        end
      end

      describe 'DELETE #remove' do
        it 'removes another user' do
          group.users.push other_user
          expect do
            delete :remove, params: { id: group.to_param, user_id: other_user.to_param }, format: :json
          end.to change(GroupsUser, :count).by(-1)
        end

        it "can't remove itself" do
          group.users.push other_user
          expect do
            delete :remove, params: { id: group.to_param, user_id: current_user.to_param }, format: :json
          end.to change(GroupsUser, :count).by(0)
        end
      end

      describe 'DELETE #leave' do
        it "can't leave the group (403)" do
          expect do
            delete :leave, params: { id: group.to_param }, format: :json
          end.to change(GroupsUser, :count).by(0)
        end
      end

      describe 'POST #invite' do
        it 'invites another user' do
          expect do
            post :invite, params: { id: group.to_param, user: { email: other_user.email } }, format: :json
          end.to change(GroupsUser, :count).by(1)
        end
      end
    end

    context 'user is not the creator of the group' do
      let(:group) { other_user.groups.first }

      before { group.users.push(current_user) }

      describe 'PUT #update' do
        it 'does not update the group' do
          old_group = group.clone

          put :update, params: { id: group.to_param, group: invalid_attributes }, session: valid_session, format: :json
          group.reload

          expect(group).to eq old_group
        end
      end

      describe 'DELETE #destroy' do
        it 'does not destroy the group' do
          expect do
            delete :destroy, params: { id: group.to_param }, session: valid_session, format: :json
          end.to change(Group, :count).by 0
        end
      end

      describe 'DELETE #remove' do
        it 'does not remove the user' do
          expect do
            delete :remove, params: { id: group.to_param, user_id: other_user.to_param }, format: :json
          end.to change(GroupsUser, :count).by(0)
        end
      end

      describe 'DELETE #leave' do
        it 'leaves the group' do
          expect do
            delete :leave, params: { id: group.to_param }, format: :json
          end.to change(GroupsUser, :count).by(-1)
        end
      end

      describe 'POST #invite' do
        it 'does not invite another user' do
          expect do
            post :invite, params: { id: group.to_param, user: { email: other_user.to_param } }, format: :json
          end.to change(GroupsUser, :count).by(0)
        end
      end
    end
  end
end
