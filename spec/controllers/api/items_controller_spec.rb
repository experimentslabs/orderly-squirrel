require 'rails_helper'

RSpec.describe Api::ItemsController, type: :controller do
  let(:current_user) { FactoryBot.create :user_active }
  let(:group) { current_user.groups.first }

  let(:valid_attributes) do
    FactoryBot.build(:item).attributes.symbolize_keys
  end
  let(:invalid_attributes) do
    { name: nil }
  end
  let(:new_attributes) do
    { name: 'Best item in the world' }
  end
  let(:valid_session) { {} }

  before do
    sign_in current_user
  end

  context 'with a group in which the user is' do
    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new item' do
          expect do
            post :create, params: { group_id: group.to_param, item: valid_attributes }, session: valid_session, format: :json
          end.to change(Item, :count).by 1
        end
      end

      context 'with invalid params' do
        it 'does not create a new item' do
          expect do
            post :create, params: { group_id: group.to_param, item: invalid_attributes }, session: valid_session, format: :json
          end.to change(Item, :count).by 0
        end
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        it 'updates the requested item' do
          item = FactoryBot.create :item, group: group, user: current_user
          put :update, params: { group_id: group.to_param, id: item.to_param, item: new_attributes }, session: valid_session, format: :json
          item.reload
          expect(item.name).to eq 'Best item in the world'
        end
      end

      context 'with invalid params' do
        it 'does not update the item' do
          item     = FactoryBot.create :item, group: group, user: current_user
          old_item = item.clone

          put :update, params: { group_id: group.to_param, id: item.to_param, item: invalid_attributes }, session: valid_session, format: :json
          item.reload

          expect(item).to eq old_item
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested item' do
        item = FactoryBot.create :item, group: group, user: current_user
        expect do
          delete :destroy, params: { group_id: group.to_param, id: item.to_param }, session: valid_session, format: :json
        end.to change(Item, :count).by(-1)
      end
    end
  end
end
