# Preview all emails at http://localhost:3000/rails/mailers/notifications
class NotificationsPreview < ActionMailer::Preview
  def assigned_to_item
    NotificationsMailer.with(shopping_lists_item: ShoppingListsItem.first).assigned_to_item
  end
end
