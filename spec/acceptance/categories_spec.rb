require 'acceptance_helper'

RSpec.describe 'Categories', type: :acceptance do
  resource 'Categories', 'Manage group categories'

  entity :category,
         id:         { type: :integer, description: 'Category identifier' },
         name:       { type: :string, description: 'Category name' },
         group_id:   { type: :integer, description: 'Concerned group' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             name:  { type: :array, required: false, description: 'Array of errors for the name field' },
             group: { type: :array, required: false, description: 'Array of errors for the group' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'Concerned group identifier' },
             id:       { type: :integer, description: 'Category identifier' }

  parameters :category_request_params,
             category: {
               type: :object, required: true, properties: {
                 name: { type: :string, required: true, description: 'The name' },
               }
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:category) { FactoryBot.create :category, user: current_user, group: group }

  let(:id) { category.id }
  let(:group_id) { group.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/categories', 'Categories defined in the given group' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      FactoryBot.create_list :category, 2, user: current_user, group: group
      visit url
      expect(response).to have_many defined :category
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/categories', 'Creates a category' do
    path_params defined: :group_path_params

    request_params defined: :category_request_params

    for_code 201 do |url|
      visit url, payload: { category: { name: 'New category' } }
      expect(response).to have_one defined :category
    end

    for_code 422 do |url|
      visit url, payload: { category: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/categories/:id', 'Display a category' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :category
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/categories/:id', 'Updates a category' do
    path_params defined: :common_path_params

    request_params defined: :category_request_params

    for_code 200 do |url|
      visit url, payload: { category: { name: 'New name' } }
      expect(response).to have_one defined :category
    end

    for_code 422 do |url|
      visit url, payload: { category: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/categories/:id', 'Deletes a category' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
