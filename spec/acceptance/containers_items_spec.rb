require 'acceptance_helper'

RSpec.describe 'ContainersItems', type: :acceptance do
  resource 'Container items', 'Manage group items in containers'

  entity :containers_item,
         id:           { type: :integer, description: 'Association identifier' },
         container_id: { type: :integer, description: 'Container identifier' },
         item_id:      { type: :integer, description: 'Item identifier' },
         quantity:     { type: :integer, description: 'Item quantity in the container' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             item:      { type: :array, required: false, description: 'Item error messages' },
             container: { type: :array, required: false, description: 'Container error messages' },
             quantity:  { type: :array, required: false, description: 'Quantity error messages' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'Concerned group identifier' },
             id:       { type: :integer, description: 'Association identifier' }

  # Creation and update params are different
  base_containers_item_request_params = {
    container_id: { type: :integer, required: true, description: 'Container identifier' },
    quantity:     { type: :string, required: false, description: 'Amount in container' },
  }
  parameters :create_containers_item_request_params,
             containers_item: {
               type: :object, required: true, description: 'New association attributes', properties:
                     base_containers_item_request_params.merge(
                       item_id: { type: :integer, required: true, description: 'Valid item identifier' }
                     )
             }

  parameters :update_containers_item_request_params,
             containers_item: {
               type: :object, required: true, description: 'New association attributes', properties: base_containers_item_request_params
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:container) { FactoryBot.create :container, user: current_user, group: group }
  let(:item) { FactoryBot.create :item, user: current_user, group: group }
  let(:containers_item) { FactoryBot.create :containers_item, item: item, container: container }

  let(:group_id) { group.id }
  let(:id) { containers_item.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/containers_items', 'List associations' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      # we can't use create_list as we need a different item
      2.times do
        FactoryBot.create :containers_item,
                          container: container,
                          item:      FactoryBot.create(:item, group: group, user: current_user)
      end
      visit url
      expect(response).to have_many defined :containers_item
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/containers_items', 'Create an association' do
    path_params defined: :group_path_params

    request_params defined: :create_containers_item_request_params

    for_code 201 do |url|
      other_item = FactoryBot.create :item, group: group, user: current_user

      visit url, payload: { containers_item: { item_id: other_item.id, container_id: container.id } }
      expect(response).to have_one defined :containers_item
    end

    for_code 422 do |url|
      visit url, payload: { containers_item: { container_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/containers_items/:id', 'Get an association' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :containers_item
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/containers_items/:id', 'Move an item and/or change its quantity' do
    path_params defined: :common_path_params

    request_params defined: :update_containers_item_request_params

    for_code 200 do |url|
      visit url, payload: { containers_item: { quantity: 10 } }
      expect(response).to have_one defined :containers_item
    end

    for_code 422 do |url|
      visit url, payload: { containers_item: { container_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/containers_items/:id', 'Destroy an association' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
