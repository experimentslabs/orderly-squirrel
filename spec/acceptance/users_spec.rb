require 'acceptance_helper'

RSpec.describe 'Users', type: :acceptance do
  resource 'Users', 'User management'

  entity :user,
         id:                { type: :integer, description: 'The id' },
         name:              { type: :string, description: "User's name" },
         email:             { type: :string, description: "User's email" },
         avatar_50:         { type: :string, required: false, description: 'Avatar URL, 50x50' },
         avatar_150:        { type: :string, required: false, description: 'Avatar URL, 150x150' },
         created_at:        { type: :datetime, description: 'Account creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' },
         unconfirmed_email: { type: :string, required: false, description: 'Unconfirmed email address, if any' }

  entity :error,
         error: { type: :string, description: 'The error' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             email:    { type: :array, required: false, description: 'Email error messages' },
             name:     { type: :array, required: false, description: 'Name error messages' },
             password: { type: :array, required: false, description: 'Password error messages' },
             # Avatar field not documented: file support not implemented in rspec_rails_api
           }
         }

  parameters :common_path_params,
             id: { type: :integer, name: 'User id', description: 'Id of user to fetch' }

  parameters :user_request_params,
             user: {
               type: :object, required: true, properties: {
                 name:  { type: :string, required: false, description: 'Updated name' },
                 email: { type: :string, required: false, description: 'Updated email' },
               }
             }

  let(:current_user) do
    user = FactoryBot.create :user_active
    FactoryBot.create_list :user_active, 2, groups: [user.groups.first]
    user
  end

  before do
    sign_in current_user
  end

  on_get '/api/users', 'Lists users' do
    for_code 200 do |url|
      visit url
      expect(response).to have_many defined :user
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error
    end
  end

  on_get '/api/users/:id', 'Get one user' do
    path_params defined: :common_path_params

    let(:id) { current_user.groups.first.users.first.id }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :user
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end

    for_code 401 do |url|
      sign_out current_user
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end

  on_get '/api/me', 'Get current user profile' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :user
    end
  end

  on_patch '/api/me', 'Update current user profile' do
    request_params defined: :user_request_params

    for_code 200 do |url|
      visit url, payload: { user: { name: 'john' } }
      expect(response).to have_one defined :user
    end

    for_code 422 do |url|
      visit url, payload: { user: { email: nil } }
      expect(response).to have_one defined :form_errors_response
    end
  end
end
