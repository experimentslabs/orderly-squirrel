require 'acceptance_helper'

RSpec.describe 'Groups', type: :acceptance do
  resource 'Groups', 'Group management'

  entity :group,
         id:         { type: :integer, description: 'Group identifier' },
         name:       { type: :string, description: 'Group name' },
         creator_id: { type: :integer, description: 'Identifier of user who created the group' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Modification date' },
         user_ids:   { type: :array, description: 'Identifiers of the group members' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             name: { type: :array, required: false, description: 'Name error messages' },
           }
         }
  entity :invite_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             user: { type: :array, required: false, description: 'User error messages' },
           }
         }

  parameters :common_path_params,
             id: { type: :integer, description: 'The concerned group' }
  parameters :user_path_params,
             id:      { type: :integer, description: 'The concerned group' },
             user_id: { type: :integer, description: 'The concerned user' }

  parameters :group_request_params,
             group: {
               type: :object, required: true, description: 'New group object', attributes: {
                 name: { type: :string, required: true, description: 'Name of the new group' },
               }
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:id) { group.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups', 'List the groups the user have access to' do
    for_code 200 do |url|
      FactoryBot.create :group, users: [current_user]
      visit url
      expect(response).to have_many defined :group
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups', 'Create a new group' do
    request_params defined: :group_request_params

    for_code 201 do |url|
      visit url, payload: { group: { name: 'A great group' } }
      expect(response).to have_one defined :group
    end

    for_code 422 do |url|
      visit url, payload: { group: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:id', 'Display one group' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :group
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:id', 'Update one group' do
    path_params defined: :common_path_params

    request_params defined: :group_request_params

    for_code 200 do |url|
      visit url, payload: { group: { name: 'A better group name' } }
      expect(response).to have_one defined :group
    end

    for_code 422 do |url|
      visit url, payload: { group: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:id', 'Destroy one group' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:id/invite', 'Add an user in the group' do
    path_params defined: :common_path_params
    request_params attributes: { user: { type: :object, description: 'User object', properties: {
      email: { type: :string, description: 'Existing email address' },
    } } }

    for_code 201 do |url|
      user = FactoryBot.create :user_active
      visit url, payload: { user: { email: user.email } }
      expect(response).to have_one defined :group
    end

    for_code 422 do |url|
      visit url, payload: { user: { email: nil } }
      expect(response).to have_one defined :invite_errors_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:id/remove/:user_id', 'Remove one user from the group' do
    path_params defined: :user_path_params

    for_code 204 do |url|
      user = FactoryBot.create :user_active, groups: [group]
      visit url, path_params: { user_id: user.id }
    end

    for_code 404 do |url|
      visit url, path_params: { user_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { user_id: current_user.id }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url, path_params: { user_id: current_user.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:id/leave', 'Leave a group' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      other_group = FactoryBot.create :group, users: [current_user]
      visit url, path_params: { id: other_group.id }
    end

    for_code 403 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end
  end
end
