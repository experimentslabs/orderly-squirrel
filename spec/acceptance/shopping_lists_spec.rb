require 'acceptance_helper'

RSpec.describe 'ShoppingLists', type: :acceptance do
  resource 'Shopping lists', 'Manage group shopping lists'

  entity :shopping_list,
         id:         { type: :integer, description: 'Shopping list identifier' },
         name:       { type: :string, description: 'Shopping list name' },
         done_at:    { type: :datetime, required: false, description: 'Date of completion' },
         template:   { type: :boolean, description: 'Shopping list marked as template' },
         group_id:   { type: :integer, description: 'Concerned group' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             name:  { type: :array, required: false, description: 'Array of errors for the name field' },
             group: { type: :array, required: false, description: 'Array of errors for the group' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'The concerned group' },
             id:       { type: :integer, description: 'The concerned shopping list' }

  # Creation and update params are different
  base_slist_request_params = {
    name:     { type: :string, required: true, description: 'Shopping list name' },
    template: { type: :boolean, description: 'Shopping list marked as template' },
  }
  parameters :create_slist_request_params,
             shopping_list: {
               type: :object, required: true, description: 'New shopping list attributes', properties: base_slist_request_params
             }
  parameters :update_slist_request_params,
             shopping_list: {
               type: :object, required: true, description: 'New shopping list attributes', properties:
                     base_slist_request_params.merge(done: { type: :boolean, required: false, description: 'Is shopping list done ?' })
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:shopping_list) { FactoryBot.create :shopping_list, user: current_user, group: group }

  let(:group_id) { group.id }
  let(:id) { shopping_list.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/shopping_lists', 'List shopping lists' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      FactoryBot.create_list :shopping_list, 2, user: current_user, group: group

      visit url
      expect(response).to have_many defined :shopping_list
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/shopping_lists', 'Create a shopping list' do
    path_params defined: :group_path_params

    request_params defined: :create_slist_request_params

    for_code 201 do |url|
      visit url, payload: { shopping_list: { name: 'Fridge' } }
      expect(response).to have_one defined :shopping_list
    end

    for_code 422 do |url|
      visit url, payload: { shopping_list: { group_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/shopping_lists/:id', 'Display a shopping list' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :shopping_list
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/shopping_lists/:id', 'Update a shopping list' do
    path_params defined: :common_path_params

    request_params defined: :update_slist_request_params

    for_code 200 do |url|
      visit url, payload: { shopping_list: { name: 'Freezer' } }
      expect(response).to have_one defined :shopping_list
    end

    for_code 422 do |url|
      visit url, payload: { shopping_list: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/shopping_lists/:id', 'Destroy a shopping list' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
