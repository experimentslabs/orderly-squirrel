require 'acceptance_helper'

RSpec.describe 'Items', type: :acceptance do
  resource 'Items', 'Manage group items'

  entity :item,
         id:              { type: :integer, description: 'Item identifier' },
         name:            { type: :string, description: 'Item name' },
         category_id:     { type: :integer, description: 'Item category identifier', required: false },
         group_id:        { type: :integer, description: 'Concerned group' },
         created_at:      { type: :datetime, description: 'Creation date' },
         updated_at:      { type: :datetime, description: 'Modification date' },
         alert_threshold: { type: :integer, description: 'Minimum desired amount' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             name:  { type: :array, required: false, description: 'Array of errors for the name field' },
             group: { type: :array, required: false, description: 'Array of errors for the group' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'The concerned group' },
             id:       { type: :integer, description: 'The concerned item' }

  parameters :item_request_params,
             item: {
               type: :object, required: true, description: 'New item attributes', properties: {
                 name:        { type: :string, required: true, description: 'Item name' },
                 category_id: { type: :integer, required: false, description: 'Item category' },
               }
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:item) { FactoryBot.create :item, user: current_user, group: group }

  let(:group_id) { group.id }
  let(:id) { item.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/items', 'List items' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      FactoryBot.create_list :item, 2, user: current_user, group: group

      visit url
      expect(response).to have_many defined :item
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/items', 'Create an item' do
    path_params defined: :group_path_params

    request_params defined: :item_request_params

    for_code 201 do |url|
      visit url, payload: { item: { name: 'Cheese' } }
      expect(response).to have_one defined :item
    end

    for_code 422 do |url|
      visit url, payload: { item: { group_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/items/:id', 'Display an item' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :item
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/items/:id', 'Update an item' do
    path_params defined: :common_path_params

    request_params defined: :item_request_params

    for_code 200 do |url|
      visit url, payload: { item: { name: 'Lettuce' } }
      expect(response).to have_one defined :item
    end

    for_code 422 do |url|
      visit url, payload: { item: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/items/:id', 'Destroy an item' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
