require 'acceptance_helper'

RSpec.describe 'Logs', type: :acceptance do
  resource 'Logs', 'Group logs'

  entity :log,
         id:            { type: :integer, description: 'The id' },
         action:        { type: :string, description: 'The name' },
         group_id:      { type: :integer, description: 'Concerned group' },
         user_id:       { type: :integer, description: 'User who performed the action' },
         loggable_id:   { type: :integer, description: 'Concerned element' },
         loggable_type: { type: :string, description: 'Concerned element type' },
         created_at:    { type: :datetime, description: 'Creation date' },
         updated_at:    { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'The error' }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'Target group identifier' },
             id:       { type: :integer, description: 'Log entry id' }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:log) do
    FactoryBot.create :container, user: current_user, group: group
    Log.last
  end

  let(:group_id) { group.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/logs', 'Logs for the given group' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      FactoryBot.create_list :container, 2, user: current_user, group: group

      visit url
      expect(response).to have_many defined :log
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/logs/:id', 'Get one log entry' do
    path_params defined: :common_path_params
    let(:id) { log.id }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :log
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
