require 'acceptance_helper'

RSpec.describe 'ShoppingListsItems', type: :acceptance do
  resource 'Shopping list items', 'Manage items in shopping lists'

  entity :shopping_list_item,
         id:               { type: :integer, description: 'Association identifier' },
         shopping_list_id: { type: :integer, description: 'Container identifier' },
         item_id:          { type: :integer, description: 'Item identifier' },
         assignee_id:      { type: :integer, required: false, description: 'User assigned to this item' },
         quantity:         { type: :integer, description: 'Item quantity in the shopping list' },
         done:             { type: :boolean, description: 'Determine if item is picked/bought/whatever' },
         created_at:       { type: :datetime, description: 'Creation date' },
         updated_at:       { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             item:          { type: :array, required: false, description: 'Item error messages' },
             shopping_list: { type: :array, required: false, description: 'Shopping list error messages' },
             quantity:      { type: :array, required: false, description: 'Quantity error messages' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'The concerned group' },
             id:       { type: :integer, description: 'The concerned association' }

  # Creation and update params are different
  base_sli_request_params = {
    shopping_list_id: { type: :integer, required: true, description: 'Valid shopping list identifier' },
    quantity:         { type: :string, required: false, description: 'Desired quantity' },
  }
  parameters :create_sli_request_params,
             shopping_lists_item: {
               type: :object, required: true, description: 'Association attributes', properties:
                     base_sli_request_params.merge(item_id: { type: :integer, required: true, description: 'Valid item identifier' })
             }
  parameters :update_sli_request_params,
             shopping_lists_item: {
               type: :object, required: true, description: 'Association attributes', properties:
                     base_sli_request_params.merge(done: { type: :boolean, description: 'Determine if item is picked/bought/whatever' })
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:shopping_list) { FactoryBot.create :shopping_list, user: current_user, group: group }
  let(:item) { FactoryBot.create :item, user: current_user, group: group }
  let(:shopping_lists_item) { FactoryBot.create :shopping_lists_item, item: item, shopping_list: shopping_list }

  let(:group_id) { group.id }
  let(:id) { shopping_lists_item.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/shopping_lists_items', 'List associations' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      # we can't use create_list as we need a different item
      2.times do
        FactoryBot.create :shopping_lists_item,
                          shopping_list: shopping_list,
                          item:          FactoryBot.create(:item, group: group, user: current_user)
      end
      visit url
      expect(response).to have_many defined :shopping_list_item
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/shopping_lists_items', 'Create an association' do
    path_params defined: :group_path_params

    request_params defined: :create_sli_request_params

    for_code 201 do |url|
      other_item = FactoryBot.create :item, group: group, user: current_user
      visit url, payload: { shopping_lists_item: { item_id: other_item.id, shopping_list_id: shopping_list.id } }
      expect(response).to have_one defined :shopping_list_item
    end

    for_code 422 do |url|
      visit url, payload: { shopping_lists_item: { item_id: 0, shopping_list_id: shopping_list.id } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/shopping_lists_items/:id', 'Display an association' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :shopping_list_item
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/shopping_lists_items/:id', 'Move an item and/or change its quantity' do
    path_params defined: :common_path_params

    request_params defined: :update_sli_request_params

    for_code 200 do |url|
      visit url, payload: { shopping_lists_item: { quantity: 10 } }
      expect(response).to have_one defined :shopping_list_item
    end

    for_code 422 do |url|
      visit url, payload: { shopping_lists_item: { shopping_list_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/shopping_lists_items/:id', 'Destroy an association' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
