require 'acceptance_helper'

RSpec.describe 'Containers', type: :acceptance do
  resource 'Containers', 'Manage group containers'

  entity :container,
         id:         { type: :integer, description: 'Container identifier' },
         name:       { type: :string, description: 'Container name' },
         group_id:   { type: :integer, description: 'Concerned group' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Modification date' }

  entity :error_response,
         error: { type: :string, description: 'Error message' }
  entity :form_errors_response,
         errors: {
           type: :object, description: 'Error messages by field', attributes: {
             name:  { type: :array, required: false, description: 'Array of errors for the name field' },
             group: { type: :array, required: false, description: 'Array of errors for the group' },
           }
         }

  parameters :group_path_params,
             group_id: { type: :integer, description: 'The concerned group' }
  parameters :common_path_params,
             group_id: { type: :integer, description: 'The concerned group' },
             id:       { type: :integer, description: 'The concerned container' }

  parameters :container_request_params,
             container: {
               type: :object, required: true, description: 'New container attributes', properties: {
                 name: { type: :string, required: true, description: 'Container name' },
               }
             }

  let(:current_user) { FactoryBot.create :user_active }
  let(:forbidden_group) { FactoryBot.create(:user_active).groups.first }

  let(:group) { current_user.groups.first }
  let(:container) { FactoryBot.create :container, user: current_user, group: group }

  let(:group_id) { group.id }
  let(:id) { container.id }

  before do
    sign_in current_user
  end

  on_get '/api/groups/:group_id/containers', 'List containers' do
    path_params defined: :group_path_params

    for_code 200 do |url|
      FactoryBot.create_list :container, 2, user: current_user, group: group

      visit url
      expect(response).to have_many defined :container
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_post '/api/groups/:group_id/containers', 'Create a container' do
    path_params defined: :group_path_params

    request_params defined: :container_request_params

    for_code 201 do |url|
      visit url, payload: { container: { name: 'Fridge' } }
      expect(response).to have_one defined :container
    end

    for_code 422 do |url|
      visit url, payload: { container: { group_id: 0 } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { group_id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_get '/api/groups/:group_id/containers/:id', 'Display a container' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :container
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_put '/api/groups/:group_id/containers/:id', 'Update a container' do
    path_params defined: :common_path_params

    request_params defined: :container_request_params

    for_code 200 do |url|
      visit url, payload: { container: { name: 'Freezer' } }
      expect(response).to have_one defined :container
    end

    for_code 422 do |url|
      visit url, payload: { container: { name: nil } }
      expect(response).to have_one defined :form_errors_response
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end

    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end

  on_delete '/api/groups/:group_id/containers/:id', 'Destroy a container' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end
    for_code 404 do |url|
      visit url, path_params: { id: 0 }
      expect(response).to have_one defined :error_response
    end
    for_code 401 do |url|
      sign_out current_user
      visit url
      expect(response).to have_one defined :error_response
    end

    for_code 403 do |url|
      visit url, path_params: { group_id: forbidden_group.id }
      expect(response).to have_one defined :error_response
    end
  end
end
